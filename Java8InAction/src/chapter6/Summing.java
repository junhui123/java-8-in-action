package chapter6;

import static chapter6.Dish.menu;

import java.util.IntSummaryStatistics;
import java.util.stream.Collectors;

/**
 * 
 * Collectors.summingInt
 * Collectors.summarizingInt
 * long, double에 해당하는 summingLong, summarizingLong도 있음
 * 이를 사용한 요약 연산.
 * @author QWER
 */
public class Summing {
	public static void main(String[] args) {
		int totalCalories = menu.stream().collect(Collectors.summingInt(Dish::getCalories));
		System.out.println(totalCalories);
		
		double avgCalories = menu.stream().collect(Collectors.averagingInt(Dish::getCalories));
		System.out.println(avgCalories);
		
		IntSummaryStatistics menuStatics = menu.stream().collect(Collectors.summarizingInt(Dish::getCalories));
		System.out.println(menuStatics);
		System.out.println(menuStatics.getSum()+", "+menuStatics.getAverage());
	}
}
