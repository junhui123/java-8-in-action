package chapter6;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

import static chapter6.Dish.menu;

/**
 * Collectors.maxBy, minBy를 사용한 최대,최소값 연산
 * @author QWER
 *
 */
public class MaxMin {
	public static void main(String[] args) {
		Comparator<Dish> dishCaloriesComparator = Comparator.comparingInt(Dish::getCalories);
		Optional<Dish> mostCalorieDish = menu.stream().collect(Collectors.maxBy(dishCaloriesComparator));  
		System.out.println(mostCalorieDish.get());
		Optional<Dish> leastCalorieDish = menu.stream().collect(Collectors.minBy(dishCaloriesComparator));  
		System.out.println(leastCalorieDish.get());
	}
}
