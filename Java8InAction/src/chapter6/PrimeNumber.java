package chapter6;

import static java.util.stream.Collectors.*;import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.IntStream;

/**
 * 소수 = Prime Number
 *    = 1과 자기 자신으로만 나누어 떨어지는(나머지 0) 1보다 큰 양의 정수
 * @author QWER
 *
 */
public class PrimeNumber {
	//소수 판단
	public boolean isPrime(int candidate) {
		return IntStream.range(2, candidate) //2부터 candidate 미만 사이의 자연수 생성 
				.noneMatch(i->candidate % i == 0); //candidate로 나눌 수 없다면 참
	}
	
	//소수 판단
	public boolean isPrimeSqrt(int candidate) {
		int catidateroot = (int) Math.sqrt((double)candidate);
		return IntStream.range(2, catidateroot)  //2 부터 candidate의 제곱근 미만 사이의 자연수 생성
				.noneMatch(i->candidate % i == 0); 
	}
	
	//partitioningBy() = 분할 연산. true,false를 키로 하는 특수한 분류화.
	//boxed() = int[] 요소를 int-->Integer로 변경 하여 Integer[] 스트림을 생성 
	public Map<Boolean, List<Integer>> partitionPrimes(int n) {
		return IntStream.rangeClosed(2, n).boxed()
				.collect(partitioningBy(candidate->isPrime(candidate)));
	}
	
	public static void main(String[] args) {
		PrimeNumber pN = new PrimeNumber();
		Map<Boolean, List<Integer>> primeNumbers = pN.partitionPrimes(100);
		System.out.println(" true = "+primeNumbers.get(true));
		System.out.println("false = "+primeNumbers.get(false));
	}
}
