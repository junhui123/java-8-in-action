package chapter6;

import static chapter6.Dish.menu;
import static java.util.stream.Collectors.*;
//import java.util.stream.Collectors; 

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * 데이터 집합을 특성에 따라 분류
 * 
 * @author QWER
 *
 */
public class Grouping {
	public enum CaloricLevel { DIET, NORMAL, FAT }
	public static void main(String[] args) {
		//메뉴 그룹화
		//Dish.Type을 기준으로 일치하는 결과를 추출 (Dish.Type을 키로 하는 항목을 리스트로 분류)
		Map<Dish.Type, List<Dish>> dishByType = menu.stream().collect(groupingBy(Dish::getType));
		Map<Dish.Type, List<Dish>> dishByType2 = menu.stream().collect(groupingBy(Dish::getType, toList()));
		System.out.println(dishByType);
		System.out.println(dishByType2);
		
		Map<CaloricLevel, List<Dish>> dishByCalroicLevel = menu.stream().collect(groupingBy(
				dish -> {
					if (dish.getCalories() <= 400) {
						return CaloricLevel.DIET;
					} else if (dish.getCalories() <= 700) {
						return CaloricLevel.NORMAL;
					} else {
						return CaloricLevel.FAT;
					}
				}
			));
		System.out.println(dishByCalroicLevel);
		
		//다수준 그룹화 groupBy 여러번 사용
		Map<Dish.Type, Map<CaloricLevel, List<Dish>>> dishByTypeCaloriceLevel = menu.stream().collect(
				groupingBy(Dish::getType,
					groupingBy(
								dish -> {
									if (dish.getCalories() <= 400) {
										return CaloricLevel.DIET;
									} else if (dish.getCalories() <= 700) {
										return CaloricLevel.NORMAL;
									} else {
										return CaloricLevel.FAT;
									}
								})
							));
		System.out.println(dishByTypeCaloriceLevel);
		
		//서브그룹으로 데이터 수집
		Map<Dish.Type, Long> typesCount = menu.stream().collect(groupingBy(Dish::getType, counting()));
		System.out.println(typesCount);
		
		//메뉴에서 타입별로 가장 칼로리가 높은 음식
		//Comparator.comparingInt == Interface Comparator static Method comparingInt 
		Map<Dish.Type, Optional<Dish>> mostCaloriceByType = menu.stream().collect(
				groupingBy(Dish::getType, 
						maxBy(Comparator.comparingInt(Dish::getCalories))));
		System.out.println(mostCaloriceByType);
		
		//그룹화 결과 형 변환. Optional 삭제
		//Collectors.collectingAndThen(적용할 컬렉터, 변환함수)
		Map<Dish.Type, Dish> mostCaloricByType_RemoveOptional = menu.stream().collect(
				groupingBy(Dish::getType, 
						collectingAndThen(
								maxBy(Comparator.comparingInt(Dish::getCalories)),
								Optional::get)
						));
		System.out.println(mostCaloricByType_RemoveOptional);
		
		
		Map<Dish.Type, Integer> totalCaloriesByType = menu.stream().collect(groupingBy(Dish::getType, summingInt(Dish::getCalories)));
		System.out.println(totalCaloriesByType);
		
		Map<Dish.Type, Set<CaloricLevel>> calroriceLevelsByType= menu.stream().collect(
				groupingBy(Dish::getType,
					mapping(
							dish -> {
								if (dish.getCalories() <= 400) {
									return CaloricLevel.DIET;
								} else if (dish.getCalories() <= 700) {
									return CaloricLevel.NORMAL;
								} else {
									return CaloricLevel.FAT;
								}
							},
							toSet()) //HashSet으로 내부적으로 구현됨
						));
		System.out.println(calroriceLevelsByType);
		
		Map<Dish.Type, Set<CaloricLevel>> calroriceLevelsByTypeUse_toCollectoion= menu.stream().collect(
				groupingBy(Dish::getType,
					mapping(
							dish -> {
								if (dish.getCalories() <= 400) {
									return CaloricLevel.DIET;
								} else if (dish.getCalories() <= 700) {
									return CaloricLevel.NORMAL;
								} else {
									return CaloricLevel.FAT;
								}
							},
							toCollection(HashSet::new))
						));
		System.out.println(calroriceLevelsByTypeUse_toCollectoion);
	}
}
