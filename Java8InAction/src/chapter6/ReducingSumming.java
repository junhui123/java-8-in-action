package chapter6;

import static chapter6.Dish.menu;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Collectors.reducing
 * reducing(BinaryOperator<T> op)
 * reducing(T identity, BinaryOperator<T> op)
 * reducing(U identity, Function<T,U> mapper, BinaryOperator<U> op)
 * 
 * T/U = 시작값 또는 시작값 없을때의 반환 값(Optional<T or U>
 * Function<T, U> = 변환 함수
 * BinaryOperator<T or U> = 같은 종류의 두 항목을 하나로 합침
 *  
 * @author QWER
 */
public class ReducingSumming {
	public static void main(String[] args) {
		/**
		 * 하나의 연산을 다양한 방법을 통해 수행 가능.
		 * 컬렉터를 이용하는 코드가 더 복잡 <-> 재사용과 커스터마이징 가능성을 제공하는 높은 추상화		 
		 */
		int totalCalories = menu.stream().collect(Collectors.reducing(0, Dish::getCalories, (i,j)->i+j));
		System.out.println(totalCalories);
		
		int totalCalories2 = menu.stream().collect(Collectors.summingInt(Dish::getCalories));
		System.out.println(totalCalories2);
		
		int totalCalories3 = menu.stream().collect(Collectors.reducing(0, Dish::getCalories, Integer::sum));
		System.out.println(totalCalories3);
		
		int totalCalories4 = menu.stream().mapToInt(Dish::getCalories).sum();
		System.out.println(totalCalories4);
		
		int totalCalories5 = menu.stream().map(Dish::getCalories).reduce(Integer::sum).get();
		System.out.println(totalCalories5);
		
		Optional<Dish> mostCaloriesDish = menu.stream().collect(Collectors.reducing(
										  (d1,d2)->d1.getCalories()>d2.getCalories() ? d1: d2));
		System.out.println(mostCaloriesDish.get());
		
		Optional<Dish> mostCaloriesDish2 = menu.stream().collect(Collectors.maxBy(Comparator.comparingInt(Dish::getCalories)));  
		System.out.println(mostCaloriesDish2.get());
	}
}
