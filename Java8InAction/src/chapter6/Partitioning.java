package chapter6;

import static chapter6.Dish.menu;
import static java.util.stream.Collectors.*;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Predicate를 사용 True,False를 key값으로 하는 Map 생성
 * @author QWER
 *
 */
public class Partitioning {
	public static void main(String[] args) {
		
		//partiontiongBy(Predicate)를 사용한 분할
		//Dish::isVegetarian == true, false 반환 Predicate
		Map<Boolean, List<Dish>> partitionedMenu = menu.stream().collect(partitioningBy(Dish::isVegetarian));
		System.out.println(partitionedMenu);
		
		List<Dish> vegetarianDishes = partitionedMenu.get(true);
		System.out.println(vegetarianDishes);
		
		List<Dish> vegetarishDishesByFilter = menu.stream().filter(Dish::isVegetarian).collect(toList());
		System.out.println(vegetarishDishesByFilter);
		
		//분할 장점
		//반환된 True, False에 해당하는 요소의 스트림을 모두 유지
		Map<Boolean, Map<Dish.Type, List<Dish>>> vegetarianDishesByType = menu.stream().collect(
				partitioningBy(
						Dish::isVegetarian,
						groupingBy(Dish::getType)));
		System.out.println(vegetarianDishesByType);

		//collectingAndThen(Collector, Function) = 컬렉터를 적용하여 다른 컬렉터로 반환
		//maxBy = 감싸인 컬렉터, Optional::get = 변환 함수
		Map<Boolean, Dish> mostCaloricPartionedByVegetarian = menu.stream().collect(
				partitioningBy(
						Dish::isVegetarian,
						collectingAndThen(
								maxBy(Comparator.comparingInt(Dish::getCalories)),
								Optional::get)));
		System.out.println(mostCaloricPartionedByVegetarian);
		
		//다수준 분할 (groupingBy 내에서 groupingBy를 사용하는것과 같음)
		Map<Boolean, Map<Boolean, List<Dish>>> vegetarianByCalrories = menu.stream().collect(partitioningBy(Dish::isVegetarian, partitioningBy(d->d.getCalories() > 500)));
		System.out.println(vegetarianByCalrories);
		
		Map<Boolean, Long> vegetarianByCount = menu.stream().collect(partitioningBy(Dish::isVegetarian, counting()));
		System.out.println(vegetarianByCount); 
	}
}
