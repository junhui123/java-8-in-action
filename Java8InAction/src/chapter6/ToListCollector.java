package chapter6;

import static chapter6.Dish.menu;
import static java.util.stream.Collector.Characteristics.CONCURRENT;
import static java.util.stream.Collector.Characteristics.IDENTITY_FINISH;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Collector<T, List<T>, List<T>> = Collector<T, A, R>
 * T = 스트림 항목의 요소
 * A = 누적자
 * R = 결과 객체
 * @author QWER
 */
public class ToListCollector<T> implements Collector<T, List<T>, List<T>> {
	
	/**
	 * 새로운 결과 컨테이너 생성(데이터를 담을 자료구조)
	  */
	@Override
	public Supplier<List<T>> supplier() {
		return ArrayList::new;
	}

	/**
	 * 컨테이너에 요소 추가하는 리듀싱 함수 반환
	 */
	@Override
	public BiConsumer<List<T>, T> accumulator() {
//		return (list, item) -> list.add(item);
		return List::add;
	}

	/**
	 * 스트림 탐색으로 누적된 객체를 최종결과로 반환
	 * Function<A, R> = A는 누적자, R은 결과 객체
	 * 누적자가 최종 객체와 같은 경우 변환 과정 필요 없으므로 항등함수 반환 
	 */
	@Override
	public Function<List<T>, List<T>> finisher() {
//		return t->t;
		return Function.identity();
	}
	
	/**
	 * 결과 컨테이너 병합
	 * 서로 다른 서브 스트림을 병렬로 처리할 때 누적자가 결과를 어떻게 처리할지 정의
	 * 병렬로 처리가 필요한 경우 spliterator를 사용하여 각 서브 스트림 리듀싱을 처리
	 */
	@Override
	public BinaryOperator<List<T>> combiner() {
		return (list1, list2) -> { 
			list1.addAll(list2);
			return list1;
		};
	}

	/**
	 * characteristics() = 해당 컬렉터의 연산 정의 및 병렬 리듀스 최적화 힌트 제공 
	 * UNORDERED = 리듀싱 결과는 스트림 각 요소의 방문, 누적 순서에 영향 받지 않는다.
	 * CONCURRENT = 멀티 스레드에서 accumlator() 동시에 호출하여 병렬 리듀싱
	 * 				단독 사용 시 순서에 영향 받음. UNORDERED 함게 사용 시  순서가 중요하지 않은 상황에서 병렬 리듀싱 수행 
	 * IDENTITY_FINISH = 최종 결과로 누적자 객체 사용. finisher 생략
	 */
	@Override
	public Set<Characteristics> characteristics() {
//		return Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.IDENTITY_FINISH, Collector.Characteristics.CONCURRENT));
		return Collections.unmodifiableSet(EnumSet.of(IDENTITY_FINISH, CONCURRENT));
	}
	
	public static void main(String[] args) {
		List<Dish> dishes = menu.stream().collect(Collectors.toList());
		System.out.println(dishes);
		
		List<Dish> dishesMyCollector = menu.stream().collect(new ToListCollector<Dish>());
		System.out.println(dishesMyCollector);
		
		//IDENTITY_FINISH, CONCURRENT
		List<Dish> dishesNotImplCollector = menu.stream().collect(ArrayList::new, List::add, List::addAll);
		System.out.println(dishesNotImplCollector);
	}
}
