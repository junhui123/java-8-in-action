package chapter6;

import static chapter6.Dish.menu;

import java.util.stream.Collectors;

/**
 * Collectors.joinin() 사용한 문자열 연결
 * @author QWER
 */
public class Concat {
	public static void main(String[] args) {
		//내부적으로 StringBuilder 이용.
		//StringBuffer = 동기화 진행, StringBuilders는 동기화 진행 없음
		//String은 내부적으로 새로운 객체를 생성하는 부분이 성능 문제됨 (1.5 이후 StringBuilder로 컴파일되어 사용)
		String shortMenu = menu.stream().map(Dish::getName).collect(Collectors.joining());
		System.out.println(shortMenu);
		String shortMenu2 = menu.stream().map(Dish::getName).collect(Collectors.joining(", "));
		System.out.println(shortMenu2);
	}
}
