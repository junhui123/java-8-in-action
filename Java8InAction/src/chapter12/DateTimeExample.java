package chapter12;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeExample {
	public void useLocalDate() {
//		LocalDate date = LocalDate.of(2014, 3, 18);
		LocalDate date = LocalDate.parse("2014-03-18");
//		LocalDate date = LocalDate.parse(""); // DateTimeParseException: Text '' could not be parsed index 0 
		int year = date.getYear();
		Month month = date.getMonth();
		int monthValue = date.getMonthValue();
		int day  = date.getDayOfMonth();
		DayOfWeek dow = date.getDayOfWeek();
		int dowInt = dow.getValue();
		String dowValue = dow.name();
		int len = date.lengthOfMonth();
		boolean leap = date.isLeapYear();
		Print(year, month, monthValue, day, dow, dowInt, dowValue, len, leap);
	}
	
	public void useLocalDateWithTemporalField() {
		LocalDate date = LocalDate.now();
		int year=date.get(ChronoField.YEAR);
		int month=date.get(ChronoField.MONTH_OF_YEAR);
		int day=date.get(ChronoField.DAY_OF_MONTH);
		int dayOfWeek=date.get(ChronoField.DAY_OF_WEEK);
		int dayOfYear=date.get(ChronoField.DAY_OF_YEAR);
		Print(year, month, day, dayOfWeek, dayOfYear);
	}
	
	public void useLocalTime() {
//		LocalTime time = LocalTime.of(13, 45, 20); //13:45:20
		LocalTime time = LocalTime.parse("13:45:20");
		int hour = time.getHour();
		int minute = time.getMinute();
		int second = time.getSecond();
		Print(hour, minute, second);
	}
	
	//날짜와 시간 조합, LocalDate, LocalTime을 쌍으로 갖는 복합 클래스 LocalDateTime
	public void useLocalDateTime() {
		LocalDateTime dt1 = LocalDateTime.of(2014,  Month.MARCH, 18, 13, 45, 20);
		LocalDate date = dt1.toLocalDate();
		LocalTime time = dt1.toLocalTime();
		LocalDateTime dt2 = LocalDateTime.of(date, time);
		
		LocalDateTime dt3 = date.atTime(13, 45, 20);
		LocalDateTime dt4 = date.atTime(time);
		LocalDateTime dt5 = time.atDate(date);
		
		Print(dt1, dt2, dt3, dt4, dt5);
	}
	
	/**
	 * 	Instant 
	 *  기계는 주,날짜,시간,분으로 계산 어려움.
	 *  Unix epoch Time(1970. 1.1 0:0:0 UTC)을 기준으로 특정 지점까지 시간 초로 나타냄
	 *  사람이 읽을 수 있는 시간 정보를 제공하지 않음.
	 */
	public void useInstant() {
		Instant.ofEpochMilli(3000);
		Instant.ofEpochSecond(3);
		Instant.ofEpochSecond(3, 0);
		//두 번째 인수는 시간의 보정값 나노초를 의미 . 0~999,999,999
		//ofEpochSecond() 내부에서 음수가 들어오더라도 floorMod 연산을 통해 0~1000_000_000L 사이의 값으로 처리
		Instant.ofEpochSecond(3, 1_000_000_000);
		Instant.ofEpochSecond(3, -1_000_000_000);
		
		System.out.println(Instant.ofEpochSecond(3, 1_000_000));
		System.out.println(Instant.ofEpochSecond(3, -1_000_000));
		System.out.println(Instant.ofEpochSecond(3, -999_999_999));
		
//		int nos1 = (int)Math.floorMod(1_000_000_000, 1000_000_000L);
//		int nos2 = (int)Math.floorMod(-999_999_999, 1000_000_000L);
//		System.out.println(nos1+","+nos2);
		
//		UnsupportedTemporalTypeException: Unsupported field: DayOfMonth
//		int day = Instant.now().get(ChronoField.DAY_OF_MONTH);
	}
	
	/**
	 * Temporal Interface를 구현하는 Class를 사용 두 객체 사이의 지속시간 측정에 사용
	 * Duration : 두 객체 사이의 지속시간 (Second, NanoSecond)
	 * Period   : 년,월,일로 사이의 지속시간을 표현
	 * 
	 * 사용 클래스 구분
	 * Duration : LocalTime, LocalDateTime, Instant
	 * Period   : LocalDate
	 * 
	 * Instant는 기계를 위한 시간이므로 LocalTime, LocalDateTime, LocalDate 혼용 불가
	 */
	public void useDurationAndPeriod() {
		LocalTime time1 = LocalTime.of(13, 45, 20); 
		LocalTime time2 = LocalTime.now();
		LocalDateTime dateTime1 = LocalDateTime.of(2014,  Month.MARCH, 18, 13, 45, 20);
		LocalDateTime dateTime2 = LocalDateTime.now();
		Instant instant1 = Instant.ofEpochSecond(300000);
		Instant instant2 = Instant.now();
		
		Duration d1 = Duration.between(time1, time2);
		Duration d2 = Duration.between(dateTime1, dateTime2);
		Duration d3 = Duration.between(time1, dateTime2);
//		Duration d4 = Duration.between(dateTime1, time2); //LocalDateTime 변환 오류 발생 
		Duration d4 = Duration.between(dateTime1.toLocalTime(), time2); 
		Duration d5 = Duration.between(instant1, instant2);
		
		Duration threeMinutes1 = Duration.ofMinutes(3);
		Duration threeMinutes2 = Duration.of(3, ChronoUnit.MINUTES);
		
		Period tenDays1 = Period.between(LocalDate.of(2014, 3, 8), LocalDate.of(2014, 3, 18));
		Period tenDays2 = Period.ofDays(10);
		Period twoYearsSixMonthOndDay = Period.of(2, 6, 1);
		
		Print(d1, d2, d3, d4, d5);
		Print(threeMinutes1, threeMinutes2);
		Print(tenDays1, tenDays2, twoYearsSixMonthOndDay);
		
		long d1_second = d1.getSeconds();
		int d1_nanao = d1.getNano();
		long d1_chrono_second = d1.get(ChronoUnit.SECONDS);
		long d1_chrono_nano = d1.get(ChronoUnit.NANOS);
		Print(d1_second, d1_nanao, d1_chrono_second, d1_chrono_nano);
		
		/** Duration은 초, 나노초 단위의 지속시간을 의미. 년월일은 Period 사용 */
//		long d1_days = d1.get(ChronoUnit.DAYS); 
//		long d1_minute= d1.get(ChronoUnit.MINUTES);
		
		int tenDays1ToNum = tenDays1.getDays();
		int tenDays2ToNum = tenDays2.getDays();
		int twoYearsSixMonthOndDayToNum = twoYearsSixMonthOndDay.getDays();
		Print(tenDays1ToNum, tenDays2ToNum, twoYearsSixMonthOndDayToNum);
	}
	
	/**
	 * 절대적인 값에 의한 Date, Time 객체 변경
	 * 기존 객체는 유지하면서 변경되는 속성을 가지는 새로운 객체를 만들어 반환.
	 * date.withYear(2014); //인스턴스 생성은 되지만 변수에 할당되지 않아 아무일도 일어나지 않음 	
	 */
	public void adjustAbsolute_Date_Time() {
		LocalDate date1 = LocalDate.of(2014, 3, 18);
		LocalDate date2 = date1.withYear(2011);
		LocalDate date3 = date1.withDayOfMonth(25);
		LocalDate date4 = date1.withDayOfYear(365); //해당년도 기준으로 1~365일 이후 날짜, 1은 1월 1일 365는 12월 31일  
		LocalDate date5 = date1.with(ChronoField.MONTH_OF_YEAR, 9);
		Print(date1, date2, date3, date4, date5);
		
		LocalDateTime dateTime1 = LocalDateTime.of(2014, 3, 18, 13, 45, 20);
		LocalDateTime dateTime2 = dateTime1.withYear(2011);
		LocalDateTime dateTime3 = dateTime1.with(ChronoField.MONTH_OF_YEAR, 9);
		Print(dateTime1, dateTime2, dateTime3);
		
		LocalTime time1 = LocalTime.of(13, 45, 20);
		LocalTime time2 = time1.withHour(10); // 0~23 사이의 값만 가능
		LocalTime time3 = time1.withSecond(50); // 0~59 사이의 값만 가능
		Print(time1, time2, time3);
	}
	
	/**
	 * 	상대적인 값에 의한 Date, Time 객체 변경
	 */
	public void adjustRelative_Date_Time() {
		LocalDate date1 = LocalDate.of(2014, 3, 18);
		LocalDate date2 = date1.plusWeeks(1); 
		LocalDate date3 = date1.minusYears(3); 
		LocalDate date4 = date3.plus(6, ChronoUnit.MONTHS);
		Print(date1, date2, date3, date4);
		
		LocalDateTime dateTime1 = LocalDateTime.of(2014, 3, 18, 13, 45, 20);
		LocalDateTime dateTime2 = dateTime1.plusWeeks(1);
		LocalDateTime dateTime3 = dateTime1.minusYears(3);
		LocalDateTime dateTime4 = dateTime1.plusSeconds(36500);
		Print(dateTime1, dateTime2, dateTime3, dateTime4);
		
		LocalTime time1 = LocalTime.of(13, 45, 20);
		LocalTime time2 = time1.plusSeconds(30000);
		LocalTime time3 = time1.minusHours(10);
		Print(time1, time2, time3);
	}
	
	/**
	 * 복잡한 날짜 조정이 필요한 경우 (다음 주 일요일, 돌아오는 평일, 어떤 달의 마지막 날 ...)
	 * with()에 TeomporalAdjusters를 전달하여 사용
	 */
	public void useTemporalAdjusters() {
		LocalDate date1 = LocalDate.of(2014, 3, 18);
		LocalDate date2 = date1.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)); 
		LocalDate date3 = date2.with(TemporalAdjusters.lastDayOfMonth());
		Print(date1, date2, date3);
	}
	
	/**
	 * 날짜와 시간 객체 파싱 및 출력
	 */
	public void useDateTimeFommater() {
		LocalDate date = LocalDate.of(2014, 3, 18);
		String s1 = date.format(DateTimeFormatter.BASIC_ISO_DATE);
		String s2 = date.format(DateTimeFormatter.ISO_LOCAL_DATE);
		Print(s1, s2);
		
		LocalDate date1 = LocalDate.parse("20140318", DateTimeFormatter.BASIC_ISO_DATE);
		//파싱할 패턴과 일치하지 않으면 오류 발생 : DateTimeParseException: Text '20140318' could not be parsed at index 0
		//LocalDate date2 = LocalDate.parse("20140318", DateTimeFormatter.ISO_LOCAL_DATE); 
		LocalDate date2 = LocalDate.parse("2014-03-18", DateTimeFormatter.ISO_LOCAL_DATE); 
		Print(date1, date2);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String formattedDate = date1.format(formatter);
		LocalDate date3 = LocalDate.parse(formattedDate, formatter); 
		Print(formattedDate, date3);
		
		//지역화된 DateTimeFormatter
		//DateTimeFormatter italidanFormatter = DateTimeFormatter.ofPattern("d. MMMM yyyy", Locale.ITALIAN);
		//세부적으로 정의한 포메터 사용
		DateTimeFormatter italidanFormatter = createItalianDateTimeFormatter();
		String formattedDateItalian = date.format(italidanFormatter);
		LocalDate date4 = LocalDate.parse(formattedDateItalian, italidanFormatter);
		Print(formattedDateItalian, date4);
	}
	
	/**
	 * DateTimeFormatterBuilder를 통한 DateTimeFormatter 생성
	 * 복합적이고 세부적으로 포메터를 생성
	 */
	public DateTimeFormatter createItalianDateTimeFormatter() {
		DateTimeFormatter italianFormatter = new DateTimeFormatterBuilder()
				.appendText(ChronoField.DAY_OF_MONTH)
				.appendLiteral(". ")
				.appendText(ChronoField.DAY_OF_YEAR)
				.appendLiteral(" ")
				.appendText(ChronoField.YEAR)
				.parseCaseInsensitive()
				.toFormatter(Locale.ITALIAN);
		return italianFormatter;
	}
	
	/**
	 * 시간대 처리 TimeZone 대체 하는 ZoneId 
	 * ZonedDateTime - 지정한 시간대에 상대적인 시점
	 * ZoneOffset - UTC/GMT 기준으로 고정 오프셋을 통한 시간대 표현 ( ex) 뉴욕은 런던보다 5시가 느리다 )
	 */
	public void useZone() {
		ZoneId romeZone = ZoneId.of("Europe/Rome");
		ZoneId defaultZone = TimeZone.getDefault().toZoneId();
		
		LocalDate date = LocalDate.of(2014, Month.MARCH, 18);
		ZonedDateTime zdt1 = date.atStartOfDay(romeZone);
		ZonedDateTime zdt2 = date.atStartOfDay(defaultZone);
		
		LocalDateTime dateTime = LocalDateTime.of(2014, Month.MARCH, 18, 18, 13, 45);
		ZonedDateTime zdt3 = dateTime.atZone(romeZone);
		ZonedDateTime zdt4 = dateTime.atZone(defaultZone);
		Print(zdt1, zdt2, zdt3, zdt4);
		
		Instant instant = Instant.now();
		ZonedDateTime zdt5 = instant.atZone(romeZone);
		ZonedDateTime zdt6 = instant.atZone(defaultZone);
		Print(zdt5, zdt6);

		//ZoneId를 사용 Instant를 LocalDateTime으로 변경
		LocalDateTime dateTimeFromInstant1 = LocalDateTime.ofInstant(instant, romeZone);
		LocalDateTime dateTimeFromInstant2 = LocalDateTime.ofInstant(instant, defaultZone);
		Print(dateTimeFromInstant1, dateTimeFromInstant2);
		
		//ZoneOffSet을 사용하여 Instant / LocalDateTime으로 변경 가능
		//ZoneOffset extends ZoneId
		ZoneOffset newYorkOffSet = ZoneOffset.of("-05:00");
		ZonedDateTime zdt7 = dateTime.atZone(newYorkOffSet);
		Instant instantFromDateTime1 = dateTime.toInstant(newYorkOffSet);
		Print(zdt7, instantFromDateTime1);
		
		//OffsetDateTime - UTC/GMT와 ZoneOffset으로 날짜와 시간 표현
		OffsetDateTime dateTimeInNewYork = OffsetDateTime.of(dateTime, newYorkOffSet);
		Print(dateTimeInNewYork);
	}
	
	public static void main(String[] args) {
		DateTimeExample dateTime = new DateTimeExample();
		dateTime.useLocalDate();
		dateTime.useLocalDateWithTemporalField();
		dateTime.useLocalTime();
		dateTime.useLocalDateTime();
		dateTime.useInstant();
		dateTime.useDurationAndPeriod();
		dateTime.adjustAbsolute_Date_Time();
		dateTime.adjustRelative_Date_Time();
		dateTime.useTemporalAdjusters();
		dateTime.useDateTimeFommater();
		dateTime.useZone();
	}
	
	public void Print(Object... args) {
		for(Object s : args) {
			System.out.print(String.valueOf(s)+"  ");
		}
		System.out.println();
	}

}
