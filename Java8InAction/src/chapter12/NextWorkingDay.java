package chapter12;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

/**
 * 커스텀 TemporalAdjuster 
 */
public class NextWorkingDay implements TemporalAdjuster {

	@Override
	public Temporal adjustInto(Temporal temporal) {
		DayOfWeek dow = DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
		int dayToAdd = 1;
		if(dow == DayOfWeek.SATURDAY) dayToAdd = 2;
		else if(dow==DayOfWeek.FRIDAY) dayToAdd =3;
		return temporal.plus(dayToAdd, ChronoUnit.DAYS);
	}

	private static TemporalAdjuster nextWorkingDay = TemporalAdjusters.ofDateAdjuster(temporal-> {
		DayOfWeek dow = DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
		int dayToAdd = 1;
		if(dow == DayOfWeek.SATURDAY) dayToAdd = 2;
		else if(dow==DayOfWeek.FRIDAY) dayToAdd =3;
		return temporal.plus(dayToAdd, ChronoUnit.DAYS);
	});
	
	public static TemporalAdjuster getNextWorkingDayAdjuster() {
		return nextWorkingDay;
	}
	
	public static void main(String[] args) {
		LocalDate date = LocalDate.now();
		date = date.with(new NextWorkingDay());
		
		date = date.with(temporal-> {
			DayOfWeek dow = DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
			int dayToAdd = 1;
			if(dow == DayOfWeek.SATURDAY) dayToAdd = 2;
			else if(dow==DayOfWeek.FRIDAY) dayToAdd =3;
			return temporal.plus(dayToAdd, ChronoUnit.DAYS);			
		});
		
		date = date.with(NextWorkingDay.getNextWorkingDayAdjuster());
	}
}
