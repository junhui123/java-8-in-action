package chapter7;

import java.util.function.Function;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class MyParallelStream {
	public static long sequentialSum(long n) {
		return Stream.iterate(1L, i -> i + 1) //무한 스트림 생성
					.limit(n) // 무한 스트림 제한
					.reduce(0L, Long::sum); //리듀싱 연산 (모든 값을 처리하여 하나의 값으로)
	}
	
	public static long iterativeSum(long n) {
		long result = 0;
		for(long i=1L; i<=n; i++) {
			result += i;
		}
		return result;
	}
	
	public static long parallelSum(long n) {
		return Stream.iterate(1L, i->i+1)
//				.parallel() //최종 parallel 메소드만 전체 메소드 파이프라인에 영향
				.limit(n)
				.parallel() 
				.reduce(0L, Long::sum);
	}
	
	public static int getThreadPoolCounter() {
		return Runtime.getRuntime().availableProcessors();
	}
	
	public long measureSumPerf(Function<Long, Long> adder, long n) {
		long fastest = Long.MAX_VALUE;
		
		for(int i=0; i<10; i++) {
			long start = System.nanoTime();
			long sum = adder.apply(n);
			long duration = (System.nanoTime() - start ) / 1_000_000; // _ = 자릿수 표시 (java7 부터 int oldMillion = 1000000; int newMillion = 1_000_000;) 
			System.out.println("Result: " + sum);
			if(duration < fastest)  fastest = duration;
		}
		
		return fastest;
	}
	
	public static long rangedSum(long n) {
		return LongStream.rangeClosed(1, n)
				.reduce(0L, Long::sum);
	}
	
	public static long parallelRangedSum(long n) {
		return LongStream.rangeClosed(1,  n)
				.parallel()
				.reduce(0L, Long::sum);
	}

	public static class Accumulator {
		private long total = 0;

		/**
		 * 동기화로 인한 오버헤드 발생. 값은 올바르게 출력
		 * 6 msecs -> 144 msces로 성능 차이 크게 발생.
		 * @param value
		 */
//		public synchronized void add(long value) {
//			total += value;
//		}
		
		public  void add(long value) {
			total += value;
		}
		
		public  long add2(long value1, long value2) {
			total = value1+value2;
			return total;
		}
		
		//Long::add와 같음
		public  long add3(long value1, long value2) {
			return value1+value2;
		}
	}
	
	//순차 실행으로 구현
	//parallel()로 변경 시 Race Condition 발생. total값 동기화 필요
	public static long sideEffectSum(long n) {
		Accumulator accumlator = new Accumulator();
		LongStream.rangeClosed(1,  n).forEach(accumlator::add);
		return accumlator.total; 
	}
	
	//순차 실행으로 구현된 알고리즘을 parallel()을 통한 병렬 실행
	//값이 올바르게 출력되지 않음. 여러 스레드에서 add 연산을 호출. add 연산은 Atomic Operation이 아님.
	public static long sideEffectParallelSum(long n) {
		Accumulator accumlator = new Accumulator();
		LongStream.rangeClosed(1,  n).parallel().forEach(accumlator::add);
		return accumlator.total;
	}
	
	public static long sideEffectParallelSum2(long n) {
		Accumulator accumlator = new Accumulator();
		LongStream.rangeClosed(1,  n).parallel().reduce(0L, accumlator::add2);
		return accumlator.total;
	}
	
	//parallelRangedSum()과 같음.
	public static long sideEffectParallelSum3(long n) {
		Accumulator accumlator = new Accumulator();
		return LongStream.rangeClosed(1,  n).parallel().reduce(0L, accumlator::add3);
	}
	
	public static void main(String[] args) {
		MyParallelStream myParallelStream = new MyParallelStream();
		System.out.println("CPU Thread Pool Counter : "+myParallelStream.getThreadPoolCounter()+"\n");
		
		/**
		 * 박싱/언방식 오버헤드로 인해 ParallelSum의 속도가 느림
		 */
//		System.out.println("Sequential sum = " + myParallelStream.measureSumPerf(MyParallelStream::sequentialSum, 10_000_000) + " msecs");
//		System.out.println();
//		System.out.println("Iterative sum = " + myParallelStream.measureSumPerf(MyParallelStream::iterativeSum, 10_000_000) + " msecs");
//		System.out.println();
//		System.out.println("Parallel sum = " + myParallelStream.measureSumPerf(MyParallelStream::parallelSum, 10_000_000) + " msecs");
//		System.out.println();
		
		/**
		 * Side Effect가 있는 스트림. 병렬 스트림이 올바르게 동작 하려면 스레드간에 공유되는 가변 상태 객체 피해야 한다.
		 */
		System.out.println("Side Effect sum = " + myParallelStream.measureSumPerf(MyParallelStream::sideEffectSum, 10_000_000) + " msecs");
		System.out.println();
		System.out.println("Side Effect Parallel sum = " + myParallelStream.measureSumPerf(MyParallelStream::sideEffectParallelSum, 10_000_000) + " msecs");
		System.out.println();
		System.out.println("Side Effect Parallel sum2 = " + myParallelStream.measureSumPerf(MyParallelStream::sideEffectParallelSum2, 10_000_000) + " msecs");
		System.out.println();
		System.out.println("Side Effect Parallel sum3 = " + myParallelStream.measureSumPerf(MyParallelStream::sideEffectParallelSum3, 10_000_000) + " msecs");
		System.out.println();
		
		/**
		 * 기본형을 사용하는 스트림을 통해 박싱/언박싱 오버헤드 제거 
		 */
		System.out.println("Ranged sum = " + myParallelStream.measureSumPerf(MyParallelStream::rangedSum, 10_000_000) + " msecs");
		System.out.println();
		System.out.println("Parallel Ranged sum = " + myParallelStream.measureSumPerf(MyParallelStream::parallelRangedSum, 10_000_000) + " msecs");
	}
}
 