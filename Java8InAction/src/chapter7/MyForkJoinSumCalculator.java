package chapter7;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.LongStream;

/**
 * 병렬 스트림은 자바7에서 추가된 Fork/Join 프레임워크로 처리 된다.
 * 
 * Fork/Join 프레임워크  (ExecutorSevice 인터페이스 구현)
 * Task를 SubTask 여러개로 분할 후 결과를 합쳐서 전체 결과 생성
 * SubTask를 ForkJoin Pool(Thread Pool)로 분산 할당. 
 * 
 * ForkJoin Pool 이용을 위해 RecursiveTask<R> 필요. 
 * compute() 메소드를 통해 SubTask로 분할 및 더 이상 분할 불가능한 SubTask의 작업 처리 정의
 * @author QWER
 */
public class MyForkJoinSumCalculator extends RecursiveTask<Long> {

	private final long[] numbers;
	private final int start;
	private final int end;
	public static final long THRESHOLD = 10_000; //SubTask가 가질 최소값. 이 값 이하는 분할 불가
	
	public MyForkJoinSumCalculator(long[] numbers) {
		this(numbers, 0, numbers.length);
	}
	
	private MyForkJoinSumCalculator(long[] numbers, int start, int end) {
		this.numbers = numbers;
		this.start = start;
		this.end = end;
	}

	/**
	 * Dived & Conquer Algorithm 적용 
	 * if(Task가 더 이상 분할 불가) {
	 * 	Task 작업 처리 정의
	 * } else {
	 * 	Task를 2개로 분할.
	 *  분할된 첫번째 SubTask를 다시 재귀 호출
	 *  모든 SubTask 연산 완료까지 기다림
	 *  모든 SubTask 결과 합침
	 * }
	 */
	@Override
	protected Long compute() {
		int length = end - start;
		if(length <= THRESHOLD) //더 이상 분할 불가능한 경우. Base Case
			return computeSequentially(); 
		
		//Task 분할 
		MyForkJoinSumCalculator leftTask = new MyForkJoinSumCalculator(numbers, start, start+length/2);
		leftTask.fork(); //ForkJoin Pool의 다른 스레드로 분할한 SubTask를 비동기 실행 
		
		//분할 한 SubTask 둘 다 fork()를 호출하는 것 보다 한 쪽 작업에서 compute를 실행하는 것이 효율적
		//두 SubTask의 한 Task는 같은 스레드르를 재사용할 수 있으므로 불필요한 Task를 스레드에 할당하는 오버헤드 피할 수 있음.
		MyForkJoinSumCalculator rightTask = new MyForkJoinSumCalculator(numbers, start+length/2, end);
		Long rightResult = rightTask.compute(); //동기 실행. SubTask의 재분할.
		Long leftResult= leftTask.join(); //첫번째 SubTask의 값 기다림 
		return leftResult+rightResult; //결과 합침.
	}

	//더 이상 분할이 불가능한 Task를 처리
	private Long computeSequentially() {
		long sum = 0;
		for(int i=start; i<end; i++) 
			sum += numbers[i];
		return sum;
	}
	
	public static long forkJoinSum(long n) {
		long[] numbers = LongStream.rangeClosed(1,  n).toArray();
		ForkJoinTask<Long> task = new MyForkJoinSumCalculator(numbers);
		return ForkJoinPoolSingleton.getInstatnce().invoke(task);
		
		/**
		 * 일반적으로 ForkJoinPool을 여러번 생성하지 않는다. 싱글턴으로 저장해서 필요한 곳에서 사용한다.
		 * RecursiveTask 구현 내에서 ForkJoinPool invoke()는 순차 코드에서 병렬 계산 시작시에만 사용 
		 */
//		return new ForkJoinPool().invoke(task); 
	}
	
	public static class ForkJoinPoolSingleton{
		private ForkJoinPoolSingleton() {		}
		private static class SingletonHolder {
			static final ForkJoinPool single = new ForkJoinPool();
		}
		public static ForkJoinPool getInstatnce() {
			return SingletonHolder.single;
		}
	}
	
	public static void main(String[] args) {
		
		/**
		 * Task에서 사용할 수 있도록 long[] 변환을 하기 대문에 병렬 스트림을 사용하는 것 보다 성능은 떨어짐.
		 */
		
		long n = 100_000_000L;
		long[] numbers = LongStream.rangeClosed(1,  n).toArray();
		MyForkJoinSumCalculator forkJoinSumCalc = new MyForkJoinSumCalculator(numbers);
//		long result = forkJoinSumCalc.compute();
		long result = measurePerf(forkJoinSumCalc::compute);
		System.out.println("Result1 = " + result + " msecs");
		System.out.println();
		
//		long result2 = MyForkJoinSumCalculator.forkJoinSum(n);
		long result2 =  measurePerf(MyForkJoinSumCalculator::forkJoinSum, n);
		System.out.println("Result2 = " + result2 + " msecs");
	}
	
    public static <T, R> long measurePerf(Function<T, R> f, T input) {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            R result = f.apply(input);
            long duration = (System.nanoTime() - start) / 1_000_000;
            System.out.println("Result: " + result);
            if (duration < fastest) fastest = duration;
        }
        return fastest;
    }
    
    public static <T> long measurePerf(Supplier<T> s) {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            T result = s.get();
            long duration = (System.nanoTime() - start) / 1_000_000;
            System.out.println("Result: " + result);
            if (duration < fastest) fastest = duration;
        }
        return fastest;
    }
}
