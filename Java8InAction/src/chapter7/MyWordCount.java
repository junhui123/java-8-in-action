package chapter7;

import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MyWordCount {
	
    public static final String SENTENCE =
            " Nel   mezzo del cammin  di nostra  vita " +
            "mi  ritrovai in una  selva oscura" +
            " che la  dritta via era   smarrita ";

	//반복형
	public int countWordsIterative(String s) {
		int counter = 0;

		boolean lastSpace = true;
		for (char c : s.toCharArray()) {
			if (Character.isWhitespace(c)) {
				lastSpace = true;
			} else {
				if (lastSpace) {
					counter++;
				}
				lastSpace = false;
			}
		}
		return counter;
	}
	
	//함수형
	//String은 기본형 스트림이 없음. Stream<Character> 사용
	public Stream<Character> counterWordsFunctional(String s) {
		Stream<Character> stream = IntStream.range(0, SENTENCE.length()).mapToObj(SENTENCE::charAt);
		return stream;
	}
	
	public int countWords(Stream<Character> stream) {
		//reduce - Stream 내의 요소를 하나 하나 줄여가며 누적 연산
		//BinaryOperator<T> - 병렬처리된 결과를 하나로 합치는 연산
		//BinaryFuncton<T> - 이전 연산 결과와 스트림 요소에 수행할 연산 
		//reduce(BinaryOperator<T>)
		//reduce(초기값, BinaryOperator<T>)
		//reduce(초기값, BinaryFuncton<T,R>, BinaryOperator<T>)
		WordCounter wordCounter = stream.reduce(new WordCounter(0, true),
												WordCounter::accumulate,
												WordCounter::combine);
		return wordCounter.getCounter();
	}
	
	private static class WordCounter {
	    private final int counter;
	    private final boolean lastSpace;
		
	    public WordCounter(int counter, boolean lastSpace) {
			super();
			this.counter = counter;
			this.lastSpace = lastSpace;
		}
	    
	    //counter, lastSpace는 WordCounter가 가지는 유일한 변수들.
	    //이 두 변수는 final로 선언되어 있으므로 엄밀히 말하면 WordCounter는 불변 클래스
	    //값이 변할 때 마다 새롭게 객체 생성 필요
	    public WordCounter accumulate(Character c) {
	    	if(Character.isWhitespace(c)) {
	    		return lastSpace ? this : new WordCounter(counter, true);  
	    	} else {
	    		return lastSpace ? new WordCounter(counter+1, false) : this;
	    	}
		}
	    
	    public WordCounter combine(WordCounter wordCounter) {
	    	return new WordCounter(counter+wordCounter.counter, wordCounter.lastSpace);
	    }
	    
	    public int getCounter( ) {
	    	return counter;
	    }
	}
	
	/**
	 * class WordCounter를 병렬로 실행 시 임의의 위치에서 분할로 인한 잘못된 결과 반환
	 * 단어가 끝나는 위치에서만 분할 하도록 Spliterator를 구현한  WordCounterSpliterator 정의
	 * 
	 * (Spliterator = 탐색 하려는 데이터를 포함하는 스트림을 어떻게 병렬화 할 것인가 정의)
	 * @author QWER
	 */
	public static class WordCounterSpliterator implements Spliterator<Character> {
		private final String string;
		private int currentChar = 0;
		
		public WordCounterSpliterator(String string) {
			this.string = string;
		}

		/**
		 * 스트림을 탐색하면서 Counsumer action을 적용  
		 * WordCounter의 accumulate 적용
		 */
		@Override
		public boolean tryAdvance(Consumer<? super Character> action) {
			action.accept(string.charAt(currentChar++));
			return currentChar < string.length();
		}

		/**
		 * 반복되는 자료구조를 분할
		 * 문자열 중간 위치를 기준으로 분할 하되
		 * 단어 중간을 분리하지 않도록 공백이 나올때까지 분할 위치 이동 
		 * null은 분할 중지를 의미.
		 * 너무 많은 Task를 만들지 않도록 조절 필요.
		 */
		@Override
		public Spliterator<Character> trySplit() {
			int currentSize = string.length() - currentChar;
			if(currentSize < 10)
				return null;
			
			for(int splitPos = currentSize / 2 + currentChar; splitPos < string.length(); splitPos++) {
				if(Character.isWhitespace(string.charAt(splitPos))) {
					Spliterator<Character> spliterator = new WordCounterSpliterator(string.substring(currentChar, splitPos));
					currentChar = splitPos;
					return spliterator;
				}
			}
			return null;
		}

		@Override
		public long estimateSize() {
			return string.length() - currentChar;
		}

		@Override
		public int characteristics() {
			return ORDERED+SIZED+SUBSIZED+NONNULL+IMMUTABLE;
		}
	}
	
	public static void main(String[] args) {
		MyWordCount wordCount = new MyWordCount();
		int counterIter = wordCount.countWordsIterative(SENTENCE);
		System.out.println("Found "+counterIter+ " words");
		
		int counterFunc = wordCount.countWords(wordCount.counterWordsFunctional(SENTENCE));
		System.out.println("Found "+counterFunc+ " words");
		
		//병렬 스트림으로 실행 시 문자열을 임의의 위치에서 둘로 분할.
		//하나의 단어가 두 개의 단어로 분할 되는 상황 발생 가능. 분할의 위치에 따라 잘못된 결과 가능
		int counterFuncParallel = wordCount.countWords(wordCount.counterWordsFunctional(SENTENCE).parallel());
		System.out.println("Found "+counterFuncParallel+ " words");
		
		Spliterator<Character> spliterator = new WordCounterSpliterator(SENTENCE);
		Stream<Character> stream = StreamSupport.stream(spliterator, true); //병렬 스트림 생성
		int counterSpliterator = wordCount.countWords(stream);
		System.out.println("Found "+counterSpliterator+ " words");
		
	}	
}
