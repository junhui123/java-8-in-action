package chapter7;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.LongStream;

public class Test {
	public static void main(String[] args) {
		System.out.println("Start");
		
		System.out.println("CPU Thread Pool Counter : "+MyParallelStream.getThreadPoolCounter()+"\n");
		
		/**
		 * 박싱/언방식 오버헤드로 인해 ParallelSum의 속도가 느림
		 */
		System.out.println("Sequential sum = " + measurePerf(MyParallelStream::sequentialSum, 10_000_000) + " msecs");
		System.out.println();
		System.out.println("Iterative sum = " + measurePerf(MyParallelStream::iterativeSum, 10_000_000) + " msecs");
		System.out.println();
		System.out.println("Parallel sum = " + measurePerf(MyParallelStream::parallelSum, 10_000_000) + " msecs");
		System.out.println();
		
		/**
		 * Side Effect가 있는 스트림. 병렬 스트림이 올바르게 동작 하려면 스레드간에 공유되는 가변 상태 객체 피해야 한다.
		 */
		System.out.println("Side Effect sum = " + measurePerf(MyParallelStream::sideEffectSum, 10_000_000) + " msecs");
		System.out.println();
		System.out.println("Side Effect Parallel sum = " + measurePerf(MyParallelStream::sideEffectParallelSum, 10_000_000) + " msecs");
		System.out.println();
		System.out.println("Side Effect Parallel sum2 = " + measurePerf(MyParallelStream::sideEffectParallelSum2, 10_000_000) + " msecs");
		System.out.println();
		System.out.println("Side Effect Parallel sum3 = " + measurePerf(MyParallelStream::sideEffectParallelSum3, 10_000_000) + " msecs");
		System.out.println();
		
		/**
		 * 기본형을 사용하는 스트림을 통해 박싱/언박싱 오버헤드 제거 
		 */
		System.out.println("Ranged sum = " + measurePerf(MyParallelStream::rangedSum, 10_000_000) + " msecs");
		System.out.println();
		System.out.println("Parallel Ranged sum = " + measurePerf(MyParallelStream::parallelRangedSum, 10_000_000) + " msecs");
		System.out.println();

		/**
		 * Task에서 사용할 수 있도록 long[] 변환을 하기 대문에 병렬 스트림을 사용하는 것 보다 성능은 떨어짐.
		 */
		long n = 100_000_000L;
		long[] numbers = LongStream.rangeClosed(1,  n).toArray();
		MyForkJoinSumCalculator forkJoinSumCalc = new MyForkJoinSumCalculator(numbers);
		long result = measurePerf(forkJoinSumCalc::compute);
		System.out.println("ForkJoin sum1 = " + result + " msecs");
		System.out.println();
		
		long result2 =  measurePerf(MyForkJoinSumCalculator::forkJoinSum, n);
		System.out.println("ForkJoin sum2 = " + result2 + " msecs");
	}
	
	private static int PerfCounter = 5;
    public static <T, R> long measurePerf(Function<T, R> f, T input) {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < PerfCounter; i++) {
            long start = System.nanoTime();
            R result = f.apply(input);
            long duration = (System.nanoTime() - start) / 1_000_000;
            System.out.println("Result: " + result);
            if (duration < fastest) fastest = duration;
        }
        return fastest;
    }
    
    public static <T> long measurePerf(Supplier<T> s) {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < PerfCounter; i++) {
            long start = System.nanoTime();
            T result = s.get();
            long duration = (System.nanoTime() - start) / 1_000_000;
            System.out.println("Result: " + result);
            if (duration < fastest) fastest = duration;
        }
        return fastest;
    }
    
	public static long measurePerf(Function<Long, Long> adder, long n) {
		long fastest = Long.MAX_VALUE;
		
		for(int i=0; i<PerfCounter; i++) {
			long start = System.nanoTime();
			long sum = adder.apply(n);
			long duration = (System.nanoTime() - start ) / 1_000_000; // _ = 자릿수 표시 (java7 부터 int oldMillion = 1000000; int newMillion = 1_000_000;) 
			System.out.println("Result: " + sum);
			if(duration < fastest)  fastest = duration;
		}
		
		return fastest;
	}
}
