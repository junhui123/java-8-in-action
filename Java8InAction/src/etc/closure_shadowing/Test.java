package etc.closure_shadowing;

public class Test {
	private int number = 999;
	
	public static void main(String[] args) {
		new Test().test();
	}
	
	//아무 반환값이 없는 경우 테스트에 용이하지 않음...
	public void test() {
		int number =100;
		
		testClosure(new Runnable() {
			
			@Override
			public void run() {
				System.out.println(number); //-->100
				//System.out.println(this.number);
				//-->오류. 여기 this는 annonymous class인 Runnable object를 가리키게 됨.
				
				System.out.println(Test.this.number); //-->999
				//System.out.println(this.toString("staic"));  
				//anonymous class가 가지고 있는 메소드(상속한 메소드 포함)와 이름이 동일한 외부 메소드에 접근할 경우 shadowing이 발생한다.
				//Runnable toString()은 String을 파라미터로 받는 메소드 없음
			}
			
		});
		
		testClosure(() -> System.out.println(number)); //-->100
		testClosure(() -> System.out.println(this.number)); //-->999. Lamda 자체는 scope 없으므로 scope이 object로 확장됨.(rexical scope = static scope.)
		
		testClosure(() -> System.out.println(toString("static"))); //Lamda expression에서는 shadowing이 발생하지 않음
	}

	private static void testClosure(final Runnable runnable) {
		runnable.run();
	}

	@Override
	public String toString() {
		return new StringBuilder("Test").append("number=").append(number).append('}').toString();
	}

	public String toString(int number) {
		return "#" + number;
	}

	public String toString(String str) {
		return "str_" + str;
	}

	public static <T> String toString(int number, T value) {
		return "[" + number + "] The value is " + String.valueOf(value) + ".";
	}
	
}
