package etc.FunctionalInterface;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ConvertUse {
	public static void main(String[] args) {
		Converter<String, Integer> converter = (from) -> Integer.valueOf(from);
		Integer converted = converter.convert("123");
		System.out.println(converted);    // 123
		converter.someOherThing();
	}
}
