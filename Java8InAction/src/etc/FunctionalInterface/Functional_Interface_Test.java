package etc.FunctionalInterface;

/**
 * @FunctionalInterface 
 * 함수형 인터페이스임을 가리키는 어노테이션
 * 함수형 인터페이스 = 추상 메소드가 1개 + default, static method 여러개 가능 
 */
@FunctionalInterface
public interface Functional_Interface_Test {
//	int min(int a, int b);
	int add(int a, int b);
	default long longAdd(long a, long b) {
		return a+b;
	}
	static double doubleAdd(double a, double b) {
		return a+b;
	}
}
