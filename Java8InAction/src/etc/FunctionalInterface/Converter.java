package etc.FunctionalInterface;

@FunctionalInterface
public interface Converter<F,T> {
	T convert(F f);
	default void someOherThing() {
		System.out.println("someOherThing");
	}
}
