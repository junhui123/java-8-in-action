package etc.closure;

//클로저 = 함수 외부에 존재하는 영역의 변수에 접근하는 것 
//내부 함수에서 외부 함수의 변수에 접근하는 것
public class Test {
	public static void main(String[] args) {
		
		int number = 100; //final이 생략된 형태 ( effectively final )
		
		/**
		 * 람다식 / 익명함수 모두 외부에 선언된 int number에 접근 가능함
		 */
		
		testClosure(new Runnable() {
			@Override
			public void run() {
				//Anonymous class의 경우 레이스 컨디션 문제 발생가능
				//Anonymous class에서 number를 사용하고 다른 스레드에서 number
				//사용 시 race condition 발생
				//number = 101;
				System.out.println(number);
			}
		});
		
		//자바의 람다는 변수의 이름이 아닌 변수의 값을 넘김 
		//따라서 final or effectively final이어야지 값이 변경되자 않음
		testClosure(() -> System.out.println(number));
		
	}
	
	
	private static void testClosure(final Runnable runnable) {
		//final인 이유 어떤 스레드에서 접근할지 모르기 때문
		runnable.run();
	}
}

/**
 * 
 * 익명 클래스와 비교해서 scope의 차이, 성능 차이, 클래스 파일 생성 차이가 있음
 * 
 * 람다는 클래스 파일 생성이 없음, 접근하는 변수 scope 차이 있음, 람다식이 더 빠름
 * 
 * 익명함수는 람다식으로 변경 가능
 * 
 * 
 */
