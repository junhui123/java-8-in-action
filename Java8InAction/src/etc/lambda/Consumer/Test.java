package etc.lambda.Consumer;

import java.util.List;

import etc.lambda.Person;
import etc.lambda.PersonList;
import static etc.lambda.Consumer.PersonConsumer.*;

public class Test {
	public static void main(String[] args) {
		List<Person> persons = PersonList.personList;

		System.out.println("========== Consumer 테스트 ==========\n");

		printPersonList(persons, personPrintConsumer());
		System.out.println("\n");
		
		// Consumer 함수를 순차적으로 실행하려면 andThen 메서드를 사용한다.
		printPersonList(persons, personPrintConsumer()
				.andThen(personChangeJobConsumer())
		);
		
		System.out.println("\n\n");
	}
}
