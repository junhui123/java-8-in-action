package etc.lambda.Consumer;

import java.util.List;
import java.util.function.Consumer;

import etc.lambda.Person;

//Consumer = 람다를 입력받아 실행, 반환 없음
public class PersonConsumer {

	public static Consumer<Person> personPrintConsumer() {
		return p -> System.out.println(p);
	}

	public static Consumer<Person> personChangeJobConsumer() {

		return p -> {
			if (p.getJob().equals("PHP Programmer")) {
				p.setJob(".Net Programmer");
				System.out.println(p);
			} else if (p.getJob().equals("Java Programmer")) {
				p.setJob("Python Programmer");
				System.out.println(p);
			}
		};
	}

	public static void printPersonList(List<Person> persons, Consumer<Person> consumer) {
		for (Person p : persons) {
			consumer.accept(p);
		}
	}

}
