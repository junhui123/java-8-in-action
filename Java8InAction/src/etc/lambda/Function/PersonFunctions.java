package etc.lambda.Function;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import etc.lambda.Person;

//Function = 입력되어진 객체를 원하는 다른 객체로 변환하는 역할을 수행
public class PersonFunctions {

	public static Function<Person, String> ListAllPersons() {
		return p -> p.toString();
	}

	public static Function<String, String> ModifyAllPerson() {
		return p -> p.replace("programmer", "engineer");
	}

	// Function 인터페이스 apply 함수가 위에서 생성한 메소드를 실행
	public static List<String> convertPersonToString(List<Person> persons, Function<Person, String> function) {
		List<String> personsString = new ArrayList<String>();

		for (Person p : persons) {
			personsString.add(function.apply(p));
		}

		return personsString;
	}

	public static List<Person> applyIdentityToPersons(List<Person> persons, Function<Person, Person> function) {
		List<Person> personsList = new ArrayList<Person>();
		for (Person p : persons) {
			personsList.add(function.apply(p));
		}

		return personsList;
	}
}
