package etc.lambda.Function;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import etc.lambda.Person;
import etc.lambda.PersonList;
import static etc.lambda.Function.PersonFunctions. *;


public class Test {
	public static void main(String[] args) {
		List<Person> persons = PersonList.personList;

		System.out.println("========== Function 테스트 ==========\n");
		
		
		convertPersonToString(persons, ListAllPersons())
				.stream().sorted().forEach(System.out::println);
		
		System.out.println("\n");
		
		//andThen = Function 함수를 순차적 실행
		convertPersonToString(persons, ListAllPersons()
				.andThen(ModifyAllPerson()))
				.forEach(System.out::println);
		
		System.out.println("\n");
		
		//identity =  대상 객체의 입력 매개변수를 반환한다. 입력 그대로 반환
		applyIdentityToPersons(persons,  Function.identity())
				 .forEach(System.out::println);
		 
		System.out.println("\n\n");
	}
}
