package etc.lambda.Supplier;

import java.util.function.Supplier;
import etc.lambda.Person;

//Supplier = 객체를 생성하고 필요할 때 객체의 인스턴스를 반환
public class Test {

	public static void main(String[] args) {
		// =============== Supplier Test ===============
		System.out.println("========== Supplier 테스트 ==========\n");
		Supplier<Person> supplier = Person::new;
		Person personMsg = supplier.get();
		System.out.println(personMsg.getMsg());

		// static 함수를 호출하여 값을 불러올 수 있다.
		Supplier<String> strSupplier = Person::getStaticMsg;
		System.out.println(strSupplier.get());
		System.out.println("\n\n");

	}
}
