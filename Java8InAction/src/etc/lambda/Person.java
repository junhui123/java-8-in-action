package etc.lambda;

public class Person {

	private String firstName;
	private String lastName;
	private String job;
	private String gender;
	private int salary;
	private int age;

	public Person(String firstName, String lastName, String job, String gender, int age, int salary) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.job = job;
		this.gender = gender;
		this.age = age;
		this.salary = salary;
	}

	public Person() {
	}

	public Person(int i, int j, String string, String string2, String string3) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.job = job;
		this.gender = gender;
		this.age = age;
		this.salary = salary;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Developer [name = " + firstName + " " + lastName + ", job = " + job + ", age = " + age + ", gender = " + gender + ", salary = " + salary + "]";
	}

	public String getMsg() {
		// TODO Auto-generated method stub
		return "안녕하세요! Person의 메시지입니다.";
	}

	public static String getStaticMsg() {
		// TODO Auto-generated method stub
		return "static 메서드의 값입니다.";
	}
}
