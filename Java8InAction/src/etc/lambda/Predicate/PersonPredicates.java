package etc.lambda.Predicate;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import etc.lambda.Person;


/**
 * Predicate 함수형 인터페이스 = 조건 확인하기 위한 인터페이스 
 * 반환값은 true/false
 * @author admin
 */
public class PersonPredicates {

	//All Persons who are male and age more than 21
	public static Predicate<Person> isAdultMale() {
		return p->p.getAge() > 18 && p.getGender().equals("M");
	}

	//All Persons who are female and age more than 18
	public static Predicate<Person> isAdultFemale() {
		return p->p.getAge() > 18 && p.getGender().equals("F");
	}
	
	//All Persons whose age is more than a given age
	public static Predicate<Person> isAgeMoreThan(Integer age) {
		return p -> p.getAge() > age;
	}
	
	public static List<Person> filterPersons(List<Person> Persons, Predicate<Person> predicate) {
		return Persons.stream().filter(predicate).collect(Collectors.<Person> toList());
	}
	 
}
