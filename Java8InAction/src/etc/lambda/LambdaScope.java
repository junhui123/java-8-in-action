package etc.lambda;

public class LambdaScope {
	public static void main(String[] args) {
		Outer outer = new Outer();
		Outer.Inner inner = outer.new Inner();
		inner.method(100);
	}
}

@FunctionalInterface
interface MyFunction {
	void myMethod();
}

class Outer {
	int val = 10; // Outer.this.val
	class Inner {
		int val = 20; // this.val
		void method(int i) { //i = 암묵적 final
			int val = 30; // val = 암묵적 final 
			// i = 10; // 에러. 상수의 값을 변경할 수 없음.

			MyFunction f = () -> {
				//   i = 10;  // 에러. 상수의 값을 변경할 수 없음. 
				// val = 40;  // 에러. 상수의 값을 변경할 수 없음.
				System.out.println("               i :" + i);
				System.out.println("             val :" + val);
				System.out.println("        this.val :" + this.val);
				System.out.println("      ++this.val :" + ++this.val);
				System.out.println("  Outer.this.val :" + Outer.this.val);
				System.out.println("++Outer.this.val :" + ++Outer.this.val);
			};
			f.myMethod();
		}
	} // Inner
} // Outer
