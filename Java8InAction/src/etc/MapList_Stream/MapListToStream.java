package etc.MapList_Stream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MapListToStream {

	public static List<Map> createMapList() {
		List<Map> list = new ArrayList<>();
		
		HashMap map1=new HashMap();
		map1.put("type", "Docuemt");
		map1.put("name", "pdf1");
		map1.put("revision", "1");
		map1.put("id", "123.456.789");
		map1.put("brand", "ABC");
		
		HashMap map2=new HashMap();
		map2.put("type", "Docuemt");
		map2.put("name", "pdf2");
		map2.put("revision", "-");
		map2.put("id", "136.556.789");
		
		HashMap map3=new HashMap();
		map3.put("type", "Docuemt");
		map3.put("name", "pdf1");
		map3.put("revision", "2");
		map3.put("id", "166.776.789");
		map3.put("price", 600);
		
		HashMap map4=new HashMap();
		map4.put("type", "Docuemt");
		map4.put("name", "pdf3");
		map4.put("revision", "-");
		map4.put("id", "296.786.789");
		map4.put("price", 500);
		
		HashMap map5=new HashMap();
		map1.put("type", "Docuemt");
		map1.put("name", "pdf1");
		map1.put("revision", "3");
		map1.put("id", "193.866.789");
		map1.put("brand", "ABC");
		map1.put("price", 900);
		
		list.add(map1);
		list.add(map2);
		list.add(map3);
		list.add(map4);
		
		return list;
	}
	
	//Enovia는 제네릭을 사용하지 않은 형태가 더 많으므로  제네릭 생략
	public static List createMapList_WithOutGeneric() {
		List list = new ArrayList<>();
		
		HashMap map1=new HashMap();
		map1.put("type", "Docuemt");
		map1.put("name", "pdf1");
		map1.put("revision", "1");
		map1.put("id", "123.456.789");
		map1.put("brand", "ABC");
		
		HashMap map2=new HashMap();
		map2.put("type", "Docuemt");
		map2.put("name", "pdf2");
		map2.put("revision", "-");
		map2.put("id", "136.556.789");
		
		HashMap map3=new HashMap();
		map3.put("type", "Docuemt");
		map3.put("name", "pdf1");
		map3.put("revision", "2");
		map3.put("id", "166.776.789");
		map3.put("price", 600);
		
		HashMap map4=new HashMap();
		map4.put("type", "Docuemt");
		map4.put("name", "pdf3");
		map4.put("revision", "-");
		map4.put("id", "296.786.789");
		map4.put("price", 500);
		
		HashMap map5=new HashMap();
		map1.put("type", "Docuemt");
		map1.put("name", "pdf1");
		map1.put("revision", "3");
		map1.put("id", "193.866.789");
		map1.put("brand", "ABC");
		map1.put("price", 900);
		
		list.add(map1);
		list.add(map2);
		list.add(map3);
		list.add(map4);
		
		return list;
	}
	
	public static void main(String[] args) {
		nanoTimeTest(MapListToStream::Test_No_Generic);
		nanoTimeTest(MapListToStream::Test_With_Generic);
		System.out.println();
		System.out.println();
		nanoTimeTest(MapListToStream::Test_No_Generic);
		nanoTimeTest(MapListToStream::Test_With_Generic);
		System.out.println();
		System.out.println();
		nanoTimeTest(MapListToStream::Test_No_Generic);
		nanoTimeTest(MapListToStream::Test_With_Generic);
	}
	
	public static void Test_No_Generic() {
		List MapList = createMapList_WithOutGeneric();
		Predicate hasPrice = m->((Map) m).containsKey("price");
		List filterList =  (List) MapList.stream()
											   .filter(m->((Map) m).get("revision").equals("3"))
											   .filter(m->((Map) m).containsKey("brand"))
											   .filter(hasPrice.and(m->(int)((Map) m).get("price")>=600))
											   .collect(Collectors.toList());
	}
	
	public static void Test_With_Generic() {
		List<Map> MapList = createMapList();
		Predicate<Map> hasPrice = m->m.containsKey("price");
		List<Map> filterList = MapList.stream()
											   .filter(m->m.get("revision").equals("3"))
											   .filter(m->m.containsKey("brand"))
											   .filter(hasPrice.and(m->(int)m.get("price")>=600))
											   .collect(Collectors.toList());
	}
	
	public static void nanoTimeTest(Runnable r) {
		List<Long> averageList = new ArrayList<Long>();
		for (int i = 0; i < 100; i++) {
			long start = System.nanoTime();
			r.run();
			long duration = (System.nanoTime() - start) ;
			averageList.add(duration);
		}
		System.out.println("average time = "+averageList.stream().mapToLong(Long::longValue).average().getAsDouble());
	}
}
