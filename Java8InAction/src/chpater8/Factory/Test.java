package chpater8.Factory;

import java.util.function.Supplier;

/**
 * 람다 사용 시 생성자 파라미터가 여러개를 가지는 경우
 * 람다를 사용하기 어려움. 여러개의 파라미터를 가지는 함수형 인터페이스 생성 필요
 * @author QWER
 *
 */
public class Test {
	static void test(String name) {
		Product p = ProductFactory.createProduct(name);
		System.out.println(p.getClass().getName());
	}

	static void test_lambda(String name) {
		Product p = ProductFactory.createProduct_Lambda(name);
		System.out.println(p.getClass().getName());
	}
	
	public static void main(String[] args) {
		Test.test("loan");
		Test.test("stock");
		System.out.println();
		
		Test.test_lambda("loan");
		Test.test_lambda("stock");
		System.out.println();
		
		Supplier<Product> loanSupplier = Loan::new;
		Loan loan = (Loan) loanSupplier.get();
		System.out.println(loan.getClass().getName());
	}
}
