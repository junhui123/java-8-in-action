package chpater8.Observer;

public interface Subject {
	void registerObserver(Observer o);
	void notifyObservers(String tweet);
}
