package chpater8.Observer;

public interface Observer {
	void notify(String tweet);
}
