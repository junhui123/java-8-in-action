package chpater8.Observer;

public class NYTimes implements Observer {
	@Override
	public void notify(String tweet) {
		if(tweet != null && tweet.contains("money")) {
			System.out.println("Beaking news in NY! " + tweet);
		}
	}
}
