package chpater8.Observer;

public class Test {
	public static void main(String[] args) {
		
		Feed f = new Feed();
		f.registerObserver(new NYTimes());
		f.registerObserver(new Guardian());
		f.registerObserver(new LeMonde());
		f.notifyObservers("The queen said her favourite book is Java 8 In Action");
		
		//Use Lambda
		f.registerObserver(tweet -> {
			if(tweet != null && tweet.contains("money")) {
				System.out.println("Beaking news in NY! " + tweet);
			}
		});
		
		f.registerObserver(tweet -> {
			if(tweet != null && tweet.contains("Java")) {
				System.out.println("Best Book ... " + tweet);
			}
		});
		
		f.registerObserver(tweet -> {
			if(tweet != null && tweet.contains("queen")) {
				System.out.println("Yet another news in London... " + tweet);
			}
		});
		
		f.notifyObservers("Java 8 In Action");
		f.notifyObservers("The queen said her favourite book is Java 8 In Action");
		
	}
}
