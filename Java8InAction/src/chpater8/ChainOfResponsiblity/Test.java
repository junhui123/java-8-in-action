package chpater8.ChainOfResponsiblity;

import java.util.function.Function;
import java.util.function.UnaryOperator;

public class Test {
	
	static void test() {
		ProcessingObject<String> p1 = new HeaderTextProcessing();
		ProcessingObject<String> p2 = new SpellCheckerProcessing();
		p1.setSuccessor(p2);
		
		String result = p1.handle("Aren't labdas really sexy?!!");
		System.out.println(result);	
	}

	/**
	 * Function<T,R> andThen 메소드를 통해 람다 조합하는 방법을 사용 하여 메소드 체이닝 구현
	 */
	static void test_Lambda() {
		UnaryOperator<String> headerProcessing = text -> "From Raoul, Mario and Alan: " + text;
		UnaryOperator<String> spellCheckerProcessing = text -> text.replaceAll("labda", "lambda");
		
		Function<String, String> pipeline = headerProcessing.andThen(spellCheckerProcessing);
		String result = pipeline.apply("Aren't labdas really sexy?!!");
		System.out.println(result);
		
	}
	
	
	public static void main(String[] args) {
		Test.test();
		Test.test_Lambda();
	}
}
