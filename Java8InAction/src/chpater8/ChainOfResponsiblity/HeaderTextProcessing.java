package chpater8.ChainOfResponsiblity;

public class HeaderTextProcessing extends ProcessingObject<String> {
	@Override
	protected String handlework(String text) {
		return "From Raoul, Mario and Alan: " + text;
	}	
}
