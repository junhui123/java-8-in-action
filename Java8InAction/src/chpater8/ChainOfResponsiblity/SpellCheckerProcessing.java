package chpater8.ChainOfResponsiblity;

public class SpellCheckerProcessing extends ProcessingObject<String> {

	@Override
	protected String handlework(String text) {
		return text.replaceAll("labda", "lambda");
	}
}
