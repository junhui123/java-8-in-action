package chpater8.Strategy;

public class IsNumericStatic {
	static public boolean execute(String s) {
		return s.matches("\\d+");
	}
}
