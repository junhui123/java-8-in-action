package chpater8.Strategy;

public class Validator {
	private final ValidationStrategy strategy;
	
	public Validator(ValidationStrategy strategy) {
		this.strategy = strategy;
	}
	
	public boolean validate(String s) {
		return strategy.execute(s);
	}
	
	public static void test1() {
		Validator numericValidator = new Validator(new IsNumeric());
		boolean b1 = numericValidator.validate("aaaa");
		System.out.println(b1);

		Validator lowerCaseValidator = new Validator(new isAllLowerCase());
		boolean b2 = lowerCaseValidator.validate("bbbb");
		System.out.println(b2);		
	}
	
	public static void test2() {
		//새로운 클래스 구현 없이 람다 이용
		//코드조각(전략)을 캡슐화 new IsNumeric -> s->s.matches("[a-z]+")) 
		Validator numericValidator = new Validator(s->s.matches("[a-z]+"));
		boolean b1 = numericValidator.validate("aaaa");
		System.out.println(b1);

		Validator lowerCaseValidator = new Validator(s->s.matches("\\d+"));
		boolean b2 = lowerCaseValidator.validate("bbbb");
		System.out.println(b2);		
	}
	
	public static void test3() {
		Validator numericValidator = new Validator(IsNumericStatic::execute);
		boolean b1 = numericValidator.validate("aaaa");
		System.out.println(b1);
		
		IsNumeric numeric = new IsNumeric();
		numericValidator = new Validator(numeric::execute);
		b1 = numericValidator.validate("aaaa");
		System.out.println(b1);
		
	}
	

	
	public static void main(String[] args) {
//		Validator.test1();
//		System.out.println();
//		Validator.test2();
		
		Validator.test3();
	}
}

