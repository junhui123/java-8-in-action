package chpater8.Strategy;

public interface ValidationStrategy {
	boolean execute(String s);
}
