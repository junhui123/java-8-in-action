package chpater8.Template_Method;

import java.util.function.Consumer;

public class OnlineBankingLambda {
	//Consumer<Customer> makeCustomerHappy = 원하는 함수 인자를 받아 처리
	public void processCustomer(int id, Consumer<Customer> makeCustomerHappy) {
		Customer c = Database.getCustomerWithId(id);
		makeCustomerHappy.accept(c);
	}
	
	// dummy Customer class
    static private class Customer {
    	static String getName() {
    		return "Customer";
    	}
    }
    // dummy Datbase class
    static private class Database{
        static Customer getCustomerWithId(int id){ return new Customer();}
    }
    
    public static void main(String[] args) {
		new OnlineBankingLambda().processCustomer(1337, 
				(Customer c)->System.out.println("Hello " + c.getName()));
	}
}
