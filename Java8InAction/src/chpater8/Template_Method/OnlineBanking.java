package chpater8.Template_Method;


public abstract class OnlineBanking {
	//해야 할 알고리즘 동작의 개요
	public void processCustomer(int id) {
		Customer c= Database.getCustomerWithId(id);
		makeCustomerHapply(c);
	}
	
	//상세한 동작. 알고리즘의 핵심 수정 가능한 유연함
    abstract void makeCustomerHapply(Customer c);

	// dummy Customer class
    static private class Customer {}
    // dummy Datbase class
    static private class Database{
        static Customer getCustomerWithId(int id){ return new Customer();}
    }
    
    public static void main(String[] args) {
		
	}
}
