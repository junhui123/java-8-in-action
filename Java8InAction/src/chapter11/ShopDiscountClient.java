package chapter11;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import chapter11.ExchangeService.Money;

public class ShopDiscountClient {
	public static final List<Shop> shops = ShopClient.shops;

	private static final Executor executor = Executors.newFixedThreadPool(Math.min(shops.size(), 100), new ThreadFactory() {
		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r);
			t.setDaemon(true);
			return t;
		}
	});
	
	/**
	 * 순차적 동기 방식
	 * 가격 정보를 가져옴
	 * 문자열 파싱해서 Quote 객체 생성
	 * 가격 정보에 할인 코드 적용하는 할인 서비스 계산 
	 */
	public List<String> findPricesSync(String product) {
		return shops.stream()
				.map(shop->shop.getPrice(product))
				.map(Quote::parse)
				.map(Discount::applyDiscount)
				.collect(toList());
	}
	
	/**
	 * 동기 작업과 비동기 작업 조합
	 * CompletableFuture.supplyAsync() 비동기와
	 * thenApply(), thenCompose() 동기 작업의 조합
	 */
	public List<String> findPrices(String product) {
		List<CompletableFuture<String>> priceFutures = 
				shops.stream()
				//첫 번째 변환 결과 : Stream<CompletableFuture<String>>, 비동기 호출
				.map(shop-> 
						CompletableFuture.supplyAsync(  
								()->shop.getPrice(product), executor)) //할인전 가격을 비동기적으로 얻음
				//두 번째 변환 결과 : Stream<CompletableFuture<Quote>>, 동기 호출
				.map(future->future.thenApply(Quote::parse)) //CompletableFuture가 완전히 완료한 뒤에 호출. <-> thenApplyAsync() = 비동기 호출
				//세 번째 변환 결과 : Stream<CompletableFuture<Quote>>, 동기 호출
				.map(future->future.thenCompose(quote-> //위에서 처리한 map의 결과 quote  
								CompletableFuture.supplyAsync(
										()->Discount.applyDiscount(quote), executor))) 
				.collect(toList());
		
		return priceFutures.stream().map(CompletableFuture::join).collect(toList());
	}
	
	/**
	 * 독립 CompletableFuture/비독립 CompletableFuture 합치기 
	 * CompletableFuture 결과.thenCombine(다른 CompletableFuture 결과, BiFunction)
	 */
    public List<String> findPricesInUSD(String product) {
        List<CompletableFuture<Double>> priceFutures = new ArrayList<>();
        for (Shop shop : shops) {
            CompletableFuture<Double> futurePriceInUSD = 
            		CompletableFuture.supplyAsync(()->shop.getDoubleTypePrice(product)) //price
            			.thenCombine( //price * rate 단순 연산은 별도의 스레드를 할당하는것은 자원 낭비
            				CompletableFuture.supplyAsync( ()->ExchangeService.getRate(Money.EUR, Money.USD)), //rate
            				(price, rate)-> price*rate
            			);
            priceFutures.add(futurePriceInUSD);
        }
        
        return priceFutures.stream().map(CompletableFuture::join).map(price->"price is "+String.format("%.2f", price)).collect(toList());
    }
    
    public List<String> findPricesInUSD_Java7(String product) {
    	List<Future<Double>>  priceFeatures = new ArrayList<>();
    	ExecutorService executor = Executors.newCachedThreadPool();
        for (Shop shop : shops) {
        	final Future<Double> futureRate = executor.submit(new Callable<Double>() {
    			@Override
    			public Double call() throws Exception {
    				return ExchangeService.getRate(Money.EUR, Money.USD);
    			}
    		});
        	
        	Future<Double> futurePriceInUSD = executor.submit(new Callable<Double>() {
    			@Override
    			public Double call() throws Exception {
    				double priceInEUR = shop.getDoubleTypePrice(product);
    				return priceInEUR * futureRate.get(); 
    			}
        	});
        	priceFeatures.add(futurePriceInUSD);
        }
        
        List<String> prices = new ArrayList<>();
        for(Future<Double> price : priceFeatures) {
        	try {
				prices.add("price is "+String.format("%.2f", price.get()));
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
        }
        return prices;
    }
    
    /**
     * 가격을 출력하는 연산에서 리스트를 생성하는 부분을 제거
     */
	public void findPricesStream(String product) {
        long start = System.nanoTime();
		Stream<CompletableFuture<String>>  stream = shops.stream()
    				 .map(shop->CompletableFuture.supplyAsync(
    						 			()->shop.getPrice(product), executor))
    				 .map(future->future.thenApply(Quote::parse))
    				 .map(future->future.thenCompose(quote->
    				 		CompletableFuture.supplyAsync(
    				 					()->Discount.applyDiscount(quote), executor)));
		
		CompletableFuture[] futures = 
				stream.map(f->f.thenAccept(System.out::println)) 
					  .toArray(size->new CompletableFuture[size]);
	
		//allOf() - Future들을 받아 완료되면 void 반환. 전달된 모든 파라미터가 처리되어야 void 반환. 실행 완료를 기다림
		// <-> anyOf - Future들을 받아 Object 반환. 처음으로 완료한 Future의 값으로 동작을 완료
		CompletableFuture.allOf(futures).join();
        System.out.println("All shops have now responded in " + ((System.nanoTime() - start) / 1_000_000) + " msecs");
    }
	
	public List<String> findPricesStreamToList(String product) {
        List<CompletableFuture<String>> priceFutures = shops.stream()
									 .map(shop->CompletableFuture.supplyAsync(
									 			()->shop.getPrice(product), executor))
									 .map(future->future.thenApply(Quote::parse))
									 .map(future->future.thenCompose(quote->
									 		CompletableFuture.supplyAsync(
									 					()->Discount.applyDiscount(quote), executor)))
									 .collect(Collectors.<CompletableFuture<String>>toList());

        return priceFutures.stream()
			               .map(CompletableFuture::join)
			               .collect(Collectors.toList());
    }
	
	public static void main(String[] args) {
		String product = "myPhone27s";
		ShopDiscountClient client = new ShopDiscountClient();
		ShopClient.Test(client::findPricesSync, product);
		System.out.println();
		ShopClient.Test(client::findPrices, product);
		System.out.println();
		ShopClient.Test(client::findPricesInUSD, product);
		System.out.println();
		ShopClient.Test(client::findPricesInUSD_Java7, product);
		System.out.println();
		client.findPricesStream(product);
	}
	
	private static final Random random = new Random();
	public static void randomDelay() {
		int delay = 500 + random.nextInt(2000);
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
