package chapter11;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class Shop {

    private final String name;
    private final Random random;

    public Shop(String name) {
        this.name = name;
        random = new Random(name.charAt(0) * name.charAt(1) * name.charAt(2));
    }
    
    public static void delay() {
    	try {
    		Thread.sleep(1000L);
    	} catch(InterruptedException e) {
    		throw new RuntimeException(e);
    	}
    }

    public double calculatePrice(String product) {
        delay();
        return random.nextDouble() * product.charAt(0) + product.charAt(1);
    }
    
    public String getName() {
        return name;
    }
    
    //동기 메소드, 블록 동작. 
    public double getDoubleTypePrice(String product) {
        double price = calculatePrice(product);
        return price;
    }
    
    //동기 메소드, 블록 동작. 
    public String getPrice(String product) {
        double price = calculatePrice(product);
        Discount.Code code = Discount.Code.values()[random.nextInt(Discount.Code.values().length)];
        return String.format("%s:%.2f:%s", name, price, code);
    }
    
    /**
     * 계산결과가 반한되기를 기다리지 않고 Future를 반환 
     */
    public Future<Double> getPriceAsync(String product) {
    	//계산결과 포함될 Future
    	CompletableFuture<Double> futurePrice = new CompletableFuture<>();
    	
    	//메인 스레드가 아닌 다른 스레드에서 비동기적 수행
    	new Thread(()-> {
    		try {
    			double price = calculatePrice(product); //비동기 계산 수행(오래 걸리는 작업)
    			futurePrice.complete(price); //작업 결과가 정상적으로 종료되면 Future에 저장
//    			throw new RuntimeException("product not available"); 
    		} catch (Exception ex) {
    			futurePrice.completeExceptionally(ex); //작업 중 문제 발생 시 발생한 에러는 Future에 저장
    		}
    	}).start();
    	return futurePrice;
    }
    /**
     * 계산결과가 반한되기를 기다리지 않고 Future를 반환 
     */
    public Future<Double> getPriceAsyncSimple(String product) {
    	//supplyAsync(Supplier). 
    	//Supplier 실행해서 비동기적으로 결과를 생성
    	//ForkJoinPool의 Executor가 Supplier 실행. 두번째 파라미터로 실행 할 Executor 지정 가능
    	return CompletableFuture.supplyAsync(()->calculatePrice(product));
    }
    
}
