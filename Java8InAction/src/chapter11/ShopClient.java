package chapter11;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.function.Consumer;
import java.util.function.Function;

public class ShopClient {
	
	public static List<Shop> shops = Arrays.asList(
													new Shop("BestPrice"),
													new Shop("LestSaveBig"), 
													new Shop("MyFavoriteShop"), 
													new Shop("BuyItAll"),
													new Shop("ShopEasy")
													);
	
//	public static List<Shop> shops = Arrays.asList(new Shop("BestPrice"), new Shop("LestSaveBig"), new Shop("MyFavoriteShop"), new Shop("BuyItAll"), new Shop("BuyItPrice"),
//													new Shop("LetsBuy"), new Shop("TrashSell"), new Shop("OMG"), new Shop("IsThisWow"), new Shop("10Shop"), new Shop("11Shop"),
//													new Shop("12Shop"), new Shop("13Shop"), new Shop("14Shop"), new Shop("15Shop"),	new Shop("16Shop"), new Shop("17Shop"),
//													new Shop("18Shop"), new Shop("19Shop"),	new Shop("20Shop"), new Shop("21Shop"),	new Shop("22Shop"), new Shop("23Shop"),
//													new Shop("24Shop"),	new Shop("25Shop"));

	public static void AsyncCall(String product) {
		Shop shop = new Shop(product);
		long start = System.nanoTime();

		//상점에 가격 정보 요청
//		Future<Double> futurePrice = shop.getPriceAsync("my favorite product"); 
		Future<Double> futurePrice = shop.getPriceAsyncSimple("my favorite product"); 
		long invocationTime = ((System.nanoTime()-start) / 1_000_000);
		System.out.println("Invocation returned after "+ invocationTime+" msecs");
		
		//정보를 기다리는 동안 다른 작업 수행
		doSomethingElse();
		try {
			//가격 정보가 있으면 Future에서 가격 정보를 읽고 없으면 받을때까지 블록
			double price = futurePrice.get();
			System.out.printf("Price is %.2f%n", price);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
		long retrievalTime = ((System.nanoTime()-start) / 1_000_000 );
		System.out.println("Price returned after " + retrievalTime + " msecs");		
	}

	/**
	 * 순차적 정보 요청
	 */
	public static List<String> findPricesSync(String product) {
		return shops
					.stream()
					.map(shop->String.format("%s price is %.2f", shop.getName(), shop.getPrice(product))) //가변인자 = Object... args
					.collect(toList());
	}
	
	/**
	 *	요청을 병렬화하여 성능 개선 
	 */
	public static List<String> findPricesParallel(String product) {
		return shops
//					.stream().parallel() //스트림으로 변환 후 스트림을 병렬화
					.parallelStream() //Collection을 바로 병렬 스트림으로
					.map(shop->String.format("%s price is %.2f", shop.getName(), shop.getPrice(product))) //가변인자 = Object... args
					.collect(toList());
	}
	
	/**
	 * 스트림을 하나로 처리 == 순차 처리
	 * 스트림은 lazy evaluation. 특성을 가짐 따라서
	 * 각 상점에 요청한 정보가 순차적으로 이루어지게됨.
	 * 기존 요청 작업이 완료되어야 join이 결과를 반환하고 이후 다음 상점 요청 처리 가능
	 * 
	 * 상점1->상점1에 대한 supplyAsync -> future.join(), 상점2 호출 -> 가격 1
	 * 상점2->상점2에 대한 supplyAsync -> future.join()          -> 가격 2
	 * 
	 * 이전 요청 처리가 완전히 끝난 다음에 새로운 CompletableFuture가 처리 
	 */
	public static List<String> findPricesAsync_OneStream(String product) {
		//List<CompletableFutre>> = 각각 계산이 종료된 상점의 문자열을 포함한 리스트
		return shops.stream()
					 .map(shop->CompletableFuture.supplyAsync( //CompletableFuture로 각각의 shop 가격을 비동기 계산
							 ()->shop.getName()+" price is " + String.format("%.2f", shop.getPrice(product)))
						  )
					.map(CompletableFuture::join)
					.collect(toList());
	}
	
	/**
	 * map 연산을 나누어 2개의 스트림 파이프 라인으로 처리.
	 * |------------첫번째 파이프라인------------|  |---------------두 번째 파이프라인---------------|    
	 * 상점1->상점1에 대한 supplyAsync, 상점2 호출 ->  future1 -> future.join(), future2 처리 -> 가격 1
	 * 상점2->상점2에 대한 supplyAsync         ->   future2 -> future.join()             -> 가격 2
	 * 
	 * ComplatableFuture를 리스트로 모은 다음에 다른 작업과는 독립적으로 각자의 작업 수행
	 */
	public static List<String> findPricesAsync(String product) {
		//List<CompletableFutre>> = 각각 계산이 종료된 상점의 문자열을 포함한 리스트
		List<CompletableFuture<String>> priceFutures =
				shops.stream()
					 .map(shop->CompletableFuture.supplyAsync( //CompletableFuture로 각각의 shop 가격을 비동기 계산
							 ()->shop.getName()+" price is " + String.format("%.2f", shop.getPrice(product)))
						  )
					 .collect(toList());
		
		//List<String> 형식 반환이므로 join()을 통해서 모든 동작이 완료되길 기다림
		//get()과 달리 exception 발생 없음
		return priceFutures.stream()
				.map(CompletableFuture::join)
				.collect(toList());
	}
	
	/**
	 * 스레드 풀에서 관리하는 스레드 수에 맞는 Executor 만들어 사용.
	 * shop의 갯수보다 많은 스레드를 가지는것은 낭비. 하나의 상점에 하나의 스레드 할당
	 * 스케쥴링을 통해 스레드가 실행 가능한 만큼 동시 실행
	 * Executor로 스레드 풀의 크기를 조절하는 등 최적화 설정 가능
	 */
	private static List<String> findPriceCustomeExecutor(String product) {
		final Executor executor = Executors.newFixedThreadPool(Math.min(shops.size(), 100), new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread t = new Thread(r);
				//일반 스레드로 설정 시 이벤트를 기다리므로 프로그램이 올바르게 종료되지 않음
				//메인 스레드 종료 시 같이 종료하기 위해 데몬으로 설정
				t.setDaemon(true);
				return t;
			}
		});
		
		List<CompletableFuture<String>> priceFutures =
				shops.stream()
					 .map(shop->CompletableFuture.supplyAsync( 
							 ()->shop.getName()+" price is " + String.format("%.2f", shop.getPrice(product)), executor))
					 .collect(toList());
		
		return priceFutures.stream()
				.map(CompletableFuture::join)
				.collect(toList());		
	}
	
	public static <T, R> void Test(Function<T, R> f, T Product) {
		long start = System.nanoTime();
		System.out.println(f.apply(Product));
		long duration = ((System.nanoTime()-start) / 1_000_000);
		System.out.println("Done in " + duration + " msecs");
	}
	
	public static <T> void Test(Consumer<T> c, T Product) {
		long start = System.nanoTime();
		c.accept(Product);
		long duration = ((System.nanoTime()-start) / 1_000_000);
		System.out.println("Done in " + duration + " msecs");
	}
	
	public static void main(String[] args) {
		//1. 동기 호출을 비동기 호출로 변경 및 예외 처리
//		AsyncCall("BestShop");
		
		//2. 병렬 스트림으로 요쳥 병렬화 하여 성능 개선 
//		Test(ShopClient::findPricesSync, "myPhone27s");
		Test(ShopClient::findPricesParallel, "myPhone27s");
		
		//3. CompletableFuture로 비동기 호출 구현
//		Test(ShopClient::findPricesAsync_OneStream, "myPhone27s");
		Test(ShopClient::findPricesAsync, "myPhone27s");
		
		//4. 더 확장성이 좋은 방법
		Test(ShopClient::findPriceCustomeExecutor, "myPhone27s");
		
		/**
		 * I/O가 없는 계산 중심 = 스트림 병렬화. 구현이 간단하며 효율적(CPU 스레드 이상의 스레드를 가질 필요 없음)
		 * I/O를 기다리는 작업 = CompletableFuture 병렬화.더 많은 유연화 및 적합한 스레드 갯수 설정 가능 하지만 I/O 작업 종료 예측 어려움
		 */
		
	}
	
	public static void doSomethingElse() {
		System.out.println("");
		System.out.println("do something else start");
		int count = 1000;
		for(int i=0; i<count; i++) {
			for(int j=0; j<count; j++) {
				for(int z=0; z<count; z++) {
				}
			}
		}
		System.out.println("do something else end");
		System.out.println();
	}
}
