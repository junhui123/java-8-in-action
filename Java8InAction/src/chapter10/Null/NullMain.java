package chapter10.Null;

public class NullMain {

	/**
	 * NullPointerException 발생 프로그램 중단
	 */
	public static String getCarInsuranceName(Person person) {
		return person.getCar().getInsurance().getName();
	}
	
	/**
	 * 들여쓰기 및 중첩으로 가독성 떨어짐. 실수 확률 증가
	 */
	public static String getCarInsuranceName2(Person person) {
		if(person != null) {
			Car car = person.getCar();
			if(car!=null) {
				Insurance insurance = car.getInsurance();
				if(insurance != null) {
					return insurance.getName();
				}
			}
		}
		return "Unknown";
	}
	
	/**
	 * 메소드에 반복되는 부분이 생김.
	 */
	private static String UNKNOWN = "UnKnown";
	public static String getCarInsuranceName3(Person person) {
		if(person == null) {
			return UNKNOWN;
		}
		
		Car car = person.getCar();
		if(car == null) {
			return UNKNOWN;
		}
		
		Insurance insurance = car.getInsurance();
		if(insurance == null) {
			return UNKNOWN;
		}
		return insurance.getName();
	}
}
