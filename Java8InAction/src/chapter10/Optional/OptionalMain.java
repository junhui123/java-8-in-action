package chapter10.Optional;

import java.util.Optional;

public class OptionalMain {
	/**
	 * Stream의 map()과 비슷함.
	 * Stream의 map(Function) - 스트림 각 요소에 제공되는 함수를 적용
	 * map(Function) - Optional이 값을 포함하면 map 인수로 제공되는 함수가 값을 변경. 없다면 빈 Optional 반환
	 */
	public static Optional<String> useOptionalMap(Insurance insurance) {
		Optional<Insurance> optInsurance = Optional.ofNullable(insurance); //Null값으로 Optional 객체 생성
		Optional<String> name = optInsurance.map(Insurance::getName);		
		return name;
	}
	
	/**
	 * flatMap(Function) - 인수로 받은 함수를 스트림 각 요소에 적용하여 각각의 요소를 스트림으로 만들어 적용 후 
	 *                     적용 결과를 하나의 스트림으로 합침 (평준화)
	 *                     
	 * 평준화 과정 - 두 Optional을 합치는 것. 하나라도 null 이면 빈 Optional 생성
	 */
	public static String userOptionalflatMap(Optional<Person> person) {
		Optional<String> name =
				person.flatMap(Person::getCar)
					  .flatMap(Car::getInsurance)
					  .map(Insurance::getName);
//					  .orElse("UnKnown"); // orElse 사용 후 String으로 바로 리턴 가능
		String resultName = name.orElse("UnKnown");
		return resultName;
	}
	
	public static Insurance findCheapstInsurance(Person person, Car car) {
		Insurance inc = null;
		
		//가장 저렴한 보험로 찾는 로직이 있다고 가정
		
		return inc;
	}
	
	/**
	 * Optional 합치기
	 * 
	 * isPresent() = Optional이 값을 포함하는지 알려줌
	 * if(car.isPresent()) <--> if(car != null)
	 * null을 확인하는 코드와 다르지 않음.
	 */
	public static Optional<Insurance> nullSafeFindCheapestInsurance_NotGood(Optional<Person> person, Optional<Car> car) {
		if(person.isPresent() && car.isPresent()) {
			return Optional.of(findCheapstInsurance(person.get(), car.get())); //of() - null이 아닌 값을 Optional로 생성 
		} else {
			return Optional.empty(); //빈 Optional 생성
		}
	}
	
	/**
	 * Optional 합치기
	 * 
	 * flatMap()에 전달된 Optional p가 비어있다면 빈 Optional 반환 아니면 Optional<Insurance> 반환하는 
	 * Function의 입력으로 사용
	 * 
	 * p가 공백이 아니면 c.map 호출
	 * c.map 또한 Optional c가 비어있다면 빈 Optional 반환.
	 * 
	 * person, car 모두 빈 Optional이 아닌 경우 map()에서 findCheapstInsurance를 호출
	 */
	public static Optional<Insurance> nullSafeFindCheapestInsurance(Optional<Person> person, Optional<Car> car) {
		return person.flatMap( p->car.map(c->findCheapstInsurance(p, c)) );
	}
	
	/**
	 * Insurance insurance  = ...;
	 * if(insurance != null && "".equals(insurance.getName())) {
	 * 		sysout("ok");
	 * } 
	 * 
	 * 이와 같은 구현을 filter를 사용해서 구현
	 */
	public static void filterInsurance(Optional<Insurance> insurance) {
		 insurance.filter(i->"CambridgeInsurance".equals(i.getName()))
				 		.ifPresent(x->System.out.println("OK")); //값이 존재한다면 인수로 넘겨준 함수를 실행. 없다면 아무일도 하지 않음
	}
	
	public String getCarInsuranceName(Optional<Person> person, int minAge) {
		return person.filter(p->p.getAge() >= minAge)
					 .flatMap(Person::getCar)
					 .flatMap(Car::getInsurance)
					 .map(Insurance::getName)
					 .orElse("UnKnown");
	}
}
