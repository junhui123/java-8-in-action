package chapter10.Optional;

import java.util.Optional;

/**
 * Optional 클래스는 클래스의 변수 형식으로 사용할 것을 가정하지 않음.
 * Optional 클래스는 Serializable 인터페이스 구현하지 않음
 * 직렬화 필요시 문제 발생 가능함.
 *  
 * class TestClass {
 * 	Optional<T> optT; //불가
 *  private T t;
 *  
 *  //일반적인 변수를 Optional로 받환 하는 메소드 추가 
 *  public Optional<T> getCarAsOptional() { 
 *  	return Optional.ofNullable(t);
 *  }
 * } 
 * 
 */
public class Person {
	private Car car;
	public Optional<Car> getCarAsOptional() {
		return Optional.ofNullable(car);
	}
	
	private int age;
	public int getAge() {
		return age;
	}
	
	private Optional<Car> optCar;
	public Optional<Car> getCar() {
		return optCar;
	}
}
