package chapter10.Optional;

import java.util.Optional;

public class Car {
	private Insurance insurance;
	public Optional<Insurance> getInsuranceAsOptional() {
		return Optional.ofNullable(insurance);
	}
	
	private Optional<Insurance> optInsuracne;
	public Optional<Insurance> getInsurance( ) {
		return optInsuracne;
	}
}
