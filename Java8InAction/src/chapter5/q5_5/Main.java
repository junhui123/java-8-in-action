package chapter5.q5_5;

import static java.util.stream.Collectors.toList;
import static java.util.Comparator.comparing;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {
		
		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario", "Milan");
		Trader alan = new Trader("Alan", "Cambridge");
		Trader brian = new Trader("Brian", "Cambridge");
				
		List<Transaction> transactions = Arrays.asList(
				new Transaction(brian, 2011, 300),
				new Transaction(raoul, 2012, 1000),
				new Transaction(raoul, 2011, 400),
				new Transaction(mario, 2011, 710),
				new Transaction(mario, 2012, 700),
				new Transaction(alan, 2012, 950)
		);
		
		//1. 2011년에 일어난 모든 트랜잭션을 찾아 값을 오름차순으로 정리
		System.out.println("1. ");
		//My
//		List<Transaction> transactionbyYear2011 = 
//					transactions.stream()
//							.filter(t->t.getYear()==2011)
//							.collect(toList()); //collect(Collections.toList());
		//Book
		List<Transaction> transactionbyYear2011 = 
				transactions.stream()
						.filter(t->t.getYear()==2011)
//						.sorted() //Transaction cannot be cast to java.lang.Comparable
//						.sorted(comparing(Transaction::getValue)) //Method Reference
						.sorted(comparing((a)->a.getValue())) //Lambda
						.collect(toList()); //collect(Collections.toList());
		
		transactionbyYear2011.forEach(System.out::println); //toString()을 호출
		System.out.println("");
		
		//2. 거래자가 근무하는 모든 도시를 중복 없이 나열 하시요.
		System.out.println("2. ");
		//My
		transactions.stream()
				.map(t->t.getTrader().getCity())
				.distinct()
				.collect(toList()).forEach(System.out::println);
		
		//Book
		List<String> cities = 
				transactions.stream()
				.map(t->t.getTrader().getCity())
				.distinct()
				.collect(toList());
				
		System.out.println("");
		
		//3. Cambridge에서 근무하는 모든 거래자를 찾아서 이름 순 정렬
		System.out.println("3. ");
		//My
		List<Transaction>  workInCambridge = 
				transactions.stream()
							.filter(t->t.getTrader().getCity().equals("Cambridge"))
							.sorted((t1, t2)->t1.getTrader().getName().compareTo(t2.getTrader().getName()))
//							.distinct() //작동안함. Trader가 아닌 각각의 Transaction에 대해서 이므로 
							.collect(toList());
		workInCambridge.forEach(System.out::println);
		System.out.println("");

		//Book
		List<Trader> traders = transactions.stream()
//				.filter(t->t.getTrader().getCity().equals("Cambridge"))
//				.map(t->t.getTrader()) //map(Trader::getTrader)
				.map(t->t.getTrader()) //map(Trader::getTrader)
				.filter(t->t.getCity().equals("Cambridge"))
				.sorted((t1, t2)->t1.getName().compareTo(t2.getName()))
				.distinct()
				.collect(toList());
		traders.forEach(System.out::println);
		System.out.println("");
		
		//4. 모든 거래자의 이름을 알파벳 순으로 정렬해서 반환
		System.out.println("4. ");
		//My
		transactions.stream()
					.map(t->t.getTrader().getName())
					.distinct()
					.sorted()
					.collect(toList()).forEach(System.out::println);
		System.out.println("");
		
		//Book
		String traderStr1=
				transactions.stream()
				.map(t->t.getTrader().getName())
				.distinct()
				.sorted()
				.reduce("", (n1, n2)->n1+n2); //문자열을 반복적으로 연결해서 새로운 문자열 생성.(효율성 부족)
		System.out.println(traderStr1+"\n");
		
		String traderStr2=
				transactions.stream()
				.map(t->t.getTrader().getName())
				.distinct()
				.sorted()
				.collect(Collectors.joining());	//내부적으로 StringBuilder 이용
		System.out.println(traderStr2+"\n");
		
		//5. Milano에 거래자가 있는가?
		System.out.println("5. ");
		//My==Book
		boolean isTraderInMilano =
				transactions.stream()
							.anyMatch(t->t.getTrader().getCity().equals("Milano"));
		System.out.println(isTraderInMilano);
		System.out.println("");
		
		//6. Cambridge에 거주하는 거래자의 모든 트랙잭션값을 출력
		System.out.println("6. ");
		//My
		transactions.stream()
					.filter(t->t.getTrader().getCity().equals("Cambridge"))
					.forEach(System.out::println);
		System.out.println("");
		
		//Book
		transactions.stream()
					.filter(t->"Cambridge".equals(t.getTrader().getCity()))
					.map(Transaction::getValue)
					//.collect(toList()) 
					.forEach(System.out::println);
		System.out.println("");
		
		//7. 전체 트랜잭션 중 최댓값?
		System.out.println("7. ");
//		Optional<Transaction> transactionMax = transactions.stream()
//					.max((t1, t2)-> t1.getValue() > t2.getValue() ?  t1.getValue() : t2.getValue());
		
		//My 
		Optional<Integer> transactionMax = transactions.stream()
				.map(t->t.getValue()) //map(Transaction::getValue)
				.reduce(Integer::max);
		System.out.println(transactionMax.get());
		System.out.println("");
		
		Optional<Transaction> max1 = transactions.stream()
				.max(comparing(Transaction::getValue)); 
		System.out.println(max1.get().getValue()+"\n");
		
		//8. 전체 트랜잭션 중 최솟값?
		System.out.println("8. ");
		//My
		Optional<Integer> transactionMin = transactions.stream()
				.map(t->t.getValue())
				.reduce(Integer::min);
		System.out.println(transactionMin.get());
		System.out.println("");
		
		//Book
		Optional<Transaction> min1 = transactions.stream()
				.reduce((t1, t2)-> t1.getValue() < t2.getValue() ? t1 : t2);
		System.out.println(min1.get().getValue()+"\n");
		
		Optional<Transaction> min2 = transactions.stream()
				.min(comparing(Transaction::getValue)); 
		System.out.println(min2.get().getValue()+"\n");
		
	}
}
