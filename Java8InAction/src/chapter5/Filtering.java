package chapter5;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import chapter6.Dish;

public class Filtering {

	public static List<Dish> Internal_Iterators(List<Dish> menu) {
		List<Dish> vegetarianDishes = new ArrayList<>();
		for (Dish d : menu) {
			if (d.isVegetarian()) {
				vegetarianDishes.add(d);
			}
		}
		return vegetarianDishes;
	}

	public static List<Dish> External_Iterators(List<Dish> menu) {
		List<Dish> vegetarianDishes = menu.stream().filter(Dish::isVegetarian).collect(toList());
		return vegetarianDishes;
	}

	public static void main(String[] args) {
		List<Integer> numbers  = Arrays.asList(1,2,1,3,2,3,2,4);
		Predicate<Integer> remainder = i->Integer.remainderUnsigned(i, 2) == 0;
		numbers.stream()
//				.filter(i->i%2==0)
				.filter(remainder)
				.distinct()
				.forEach(System.out::println);	
		
		System.out.println();
		List<Dish> vegetarianDishes = External_Iterators(Dish.menu);
		vegetarianDishes.stream()
						.filter(d->d.getCalories() > 300)
						.limit(3)
						.collect(toList())
						.forEach(System.out::println);	
		
		System.out.println();
		vegetarianDishes.stream()
						.filter(d->d.getCalories() > 300)
						.skip(2)
						.collect(toList())
						.forEach(System.out::println);	
		
		System.out.println();
		Dish.menu.stream().filter(d->d.getType()== Dish.Type.MEAT).limit(2).forEach(System.out::println);	

		
	}
}
