package chapter5;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import chapter6.Dish;

/**
 * 함수형 프로그래밍에서 Fold라고 부름 
 * ->종이(스트림)을 작은 조각이 될 때까지 접는것과 비슷하다는 의미
 * 모든 스트림 요소 처리하여 하나의 값으로 도출
 */
public class Reducing {
	
	static List<Integer> numbers = Stream.iterate(1, n->n+1).limit(10).collect(toList());
	public static void allSum() {
		
		//reduce(초기값, 요소를 조합하여 하나로 만드는 함수)
		int sum = numbers.stream().reduce(0, (a,b)->a+b); 
		System.out.println(sum);
		System.out.println(numbers.stream().reduce(0, Integer::sum));
		
		//초기값이 없는 경우 Optional 객체 반환
		Optional<Integer> optSum = numbers.stream().reduce((a,b)->a+b); 
	}
	
	public static void max_min() {
		Optional<Integer> max = numbers.stream().reduce(Integer::max); //reduce((x,y)->x < y ? x : y)
		Optional<Integer> min = numbers.stream().reduce(Integer::min); //reduce((x,y)->x < y ? x : y)
	}
	
	
	public static void getDishCount() {
		System.out.println(Dish.menu.stream().count());
		
		//map-reduce 패턴
		int count = Dish.menu.stream()
				 			 .map(d->1) //menu의 각 요소를 1로 설정
				 			 .reduce(0, (a,b)->a+b);
		System.out.println(count);
	}
	
	public static void main(String[] args) {
		allSum();
		getDishCount();
	}
}
