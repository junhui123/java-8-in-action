package chapter5;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Stream;

public class Mapping {

	public static void square1() {
		List<Integer> numbers = Arrays.asList(1,2,3,4,5);
		System.out.println(numbers.stream().map(i->i*i).collect(toList()));
	}
	
	public static void square2() {
		List<Integer> numbers1 = Arrays.asList(1,2,3);
		List<Integer> numbers2 = Arrays.asList(3,4);
		
		List<int[]> pairs = numbers1.stream()
									.flatMap(i->
												numbers2.stream()
														.map(j->new int[]{i,j}))
									.collect(toList());
		
		for(int[] a : pairs) {
			System.out.println("("+a[0]+","+a[1]+")");
		}
	}
	
	public static void square3() {
		List<Integer> numbers1 = Arrays.asList(1,2,3);
		List<Integer> numbers2 = Arrays.asList(3,4);
		
		List<int[]> pairs = numbers1.stream()
									.flatMap(i->
												numbers2.stream()
														.map(j->new int[]{i,j}))
									.collect(toList());
		
		List<int[]> remainder3 = pairs.stream()
										.filter(arr -> (arr[0]+arr[1]) % 3 == 0)
										.collect(toList());
		for(int[] a : remainder3) {
			System.out.println("("+a[0]+","+a[1]+")");
		}
	}
	
	public static void square4() {
		List<Integer> numbers1 = Arrays.asList(1,2,3);
		List<Integer> numbers2 = Arrays.asList(3,4);
		
		List<int[]> pairs = numbers1.stream()
									.flatMap(i->
												numbers2.stream()
														.filter(j->(i+j)%3==0)
														.map(j->new int[]{i,j}))
									.collect(toList());
		
		for(int[] a : pairs) {
			System.out.println("("+a[0]+","+a[1]+")");
		}
	}
	
	public static void main(String[] args) {
		List<String> words = Arrays.asList("Hello", "World");
		BiFunction<String, String, String[]> f = (s1, s2)-> s1.split(s2);
		//split 결과 String[]이 반환됨
		List<String[]> split1 = words.stream().map(s->f.apply(s, "")).distinct().collect(toList());
		split1.forEach(s->Arrays.stream(s).forEach(System.out::print));
		System.out.println();
		
		List<Stream<String>> split2 = words.stream().map(s->f.apply(s, "")).map(Arrays::stream).distinct().collect(toList());
		split2.stream().forEach(System.out::print);
		System.out.println();
		
		//Stram<String> 형태로 받기 위해 flatMap 사용
		List<String> flat_Split = words.stream().map(s->f.apply(s, "")).flatMap(Arrays::stream).distinct().collect(toList());
		System.out.println(flat_Split);
		
		//square1();
		//square2();
		square3();
		square4();
	}
}
