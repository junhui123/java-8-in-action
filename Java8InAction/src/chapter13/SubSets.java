package chapter13;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubSets {

	public static List<List<Integer>> subsets(List<Integer> list) {
		if(list.isEmpty()) {
			List<List<Integer>> ans = new ArrayList<>();
			ans.add(Collections.emptyList());
			return ans;
		}
		
		Integer first = list.get(0);
		List<Integer> rest = list.subList(1, list.size());

		List<List<Integer>> subans = subsets(rest);
		List<List<Integer>> subans2 = insertAll(first, subans);
		
		return concat(subans, subans2);
	}
	
	private static List<List<Integer>> insertAll(Integer first, List<List<Integer>> subans) {
		List<List<Integer>> list = new ArrayList<>();
		
		for(List<Integer> l: subans) {
			List<Integer> copy = new ArrayList<>();
			copy.add(first);
			copy.addAll(l);
			list.add(copy);
		}
		return list;
	}

	private static List<List<Integer>> concat(List<List<Integer>> list1, List<List<Integer>> list2) {
		List<List<Integer>> copy = new ArrayList<>();
		copy.addAll(list1);
		copy.addAll(list2);
		return copy;
	}
	
	public static void main(String[] args) {
		List<Integer> intList = new ArrayList<>();
		intList.add(1);
		intList.add(4);
		intList.add(9);
		
		List<List<Integer>> subsetList = subsets(intList);
		System.out.println(subsetList);
	}
}
