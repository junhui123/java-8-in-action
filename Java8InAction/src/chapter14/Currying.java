package chapter14;

import java.util.function.DoubleUnaryOperator;

/**
 * Currying(커링)
 * - 함수의 재사용에 유용한 기법
 * - 각 단계에 하나의 인자를 가지는 함수를 생성. 함수의 인자를 하나씩 적용 
 *  f(x,y,z)=x*y+z
 *  f(3,y,z)=g(y,z)=3*y+z
 *  g(4,z)=h(z)=3*4+z
 *  h(5)=3*4+5=17
 */
public class Currying {

	/**
	 * 화씨 -> 섭씨
	 * x=변환값, f=변환요소, b=기준치 조정 요소
	 */
	public static double convertFahrenheit(double x, double f, double b) {
		return x*f+b;
	}
	
	/**
	 * KM -> Mile
	 * x*6/10
	 */
	public static double convertMile(double x) {
		return x*6/10;
	}
	
	/**
	 * 화씨->섭씨, Km->Mile 각각 변환 메소드를 따로 만드는것 보다 
	 * 커링을 활용 로직을 재활용 하여 함수 재사용
	 * f=변환 요소, b=기준치 
	 */
	public static DoubleUnaryOperator curried(double f, double b) {
		return (double x) ->x*f+b;
	}
	
	public static void main(String[] args) {
		//기존의 변환 로직을 재활용하는 코드
		DoubleUnaryOperator convertCtoF = curried(9.0/5, 32);
		DoubleUnaryOperator convertUSDtoGBP = curried(0.6, 0); //달러->파운드
		DoubleUnaryOperator convertKmtoMile = curried(0.6214, 0);
		
		double gbp = convertUSDtoGBP.applyAsDouble(1000);
		double ctof = convertCtoF.applyAsDouble(24);
		double kmtomile = convertKmtoMile.applyAsDouble(450);
		System.out.println(gbp + ", " + ctof + ", " + kmtomile);
		
		double ctof2=convertFahrenheit(24, 9.0/5, 32);
		double kmtomile2=convertMile(450);
		System.out.println(ctof2+", "+kmtomile2);
	}
}
