package chapter14.PatternMatching;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 여러개의 if-else와 switch-case를 대신 람다를 이용하여 깔끔하게 표현
 */
public class PatternMatching {
	
	/**
	 * 자료형 언랩. 특정 데이터 형식 방문 알고리즘 캡슐화
	 * 특정 데이터 형식을 방문하여 지정된 형식의 인스턴스의 멤버에 접근 처리
	 * SimplyfyExprVisitior.visit()는 다소 복잡한 검사 요소를 가짐.
	 */
	public static void useVisitorPattern() {
		BinOp bOp = new BinOp("+", new Number(5), new Number(0));
		SimplyfyExprVisitior visitor = new SimplyfyExprVisitior();
		Expr bOpVisited = visitor.visit(bOp);
		System.out.println(bOpVisited);
	}
	
	public static void main(String[] args) {
		useVisitorPattern();
		
		Expr e = new BinOp("+", new Number(5), new Number(0));
		Expr match = simplify(e);
		System.out.println(match);
	}
	
	
	interface TriFunctions<S, T, U, R> {
		R apply(S s, T t, U u);
	}
	
	static <T> T patternMatcher(Expr e, 
			TriFunctions<String, Expr, Expr, T> binopcase,
			Function<Integer, T> numcase,
			Supplier<T> defaultcase) {

		return e instanceof BinOp ?
					binopcase.apply(((BinOp)e).opname, ((BinOp)e).left, ((BinOp)e).right) :
						e instanceof Number ?
									numcase.apply(((Number)e).val) : defaultcase.get();
	}
	
	static Expr simplify(Expr e) {
		TriFunctions<String, Expr, Expr, Expr> binopcase = 
				(opname, left, right) -> {
					if("+".equals(opname)) {
						if(left instanceof Number && right instanceof Number) {
							return new Number(((Number)left).val+((Number)right).val); 
						}
					}
					return new BinOp(opname, left, right);
				};
				
		Function<Integer, Expr> numcase = val -> new Number(val);
		Supplier<Expr> defaultcase =  () -> new Number(0);
		
		return patternMatcher(e, binopcase, numcase, defaultcase);
	}
}
