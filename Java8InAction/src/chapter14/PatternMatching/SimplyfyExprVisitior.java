package chapter14.PatternMatching;

public class SimplyfyExprVisitior {
	public Expr visit(BinOp binOp) {
		if("+".equals(binOp.opname) && binOp.right instanceof Number &&  binOp.left instanceof Number) {
			Number left = (Number) binOp.left;
			Number right = (Number) binOp.right;
			return new Number(left.val+right.val); 
		}
		return binOp;
	}
}
