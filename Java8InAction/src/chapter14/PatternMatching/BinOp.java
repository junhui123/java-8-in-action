package chapter14.PatternMatching;

public class BinOp extends Expr {
	String opname;
	Expr left, right;

	public BinOp(String opname, Expr left, Expr right) {
		this.opname = opname;
		this.left = left;
		this.right = right;
	}

	@Override
	public String toString() {
		return "(" + left + " " + opname + " " + right + ")";
	}
	
	public Expr accept(SimplyfyExprVisitior v) {
		return v.visit(this);
	}
}
