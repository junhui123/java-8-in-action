package chapter14;

import java.util.HashMap;
import java.util.function.Function;

public class HigherOrderFunctions {
	
	/**
	 * First Class Function 
	 *  - 변수에 함수를 할당, 함수를 인자로 전달
	 *  - 결과로 함수를 반환
	 *  - 자료구조에 함수를 저장
	 */
	public static void FirstClassFunction() {
		
		HashMap<String, Function<String, Integer>> map = new HashMap<>();
		//변수에 함수를 할당
		Function<String, Integer> strToInt = Integer::parseInt;
		//자료구조에 함수를 저장
		map.put("mapToInt", strToInt);
		
		String x = "10";
		Integer convert = strToInt.apply(x); //함수를 인자로 전달 
		Integer conver2 = Integer.parseInt(x); 
		System.out.println(x.getClass().getName()+", "+convert.getClass().getName()+", "+conver2.getClass().getName());
	}
	
	/**
	 * Higher Order Function 
	 *  - 하나 이상의 함수를 인수로 받음
	 *  - 함수를 결과로 반환
	 */
	public static void HigherOrderFunction() {
		Function<Double, Double> xsquare = x->x*x;
		Function<Double, Double> differntilate1 = xsquare.andThen(x->x*2);
		double result1 = differntilate1.apply((double) 2);
		System.out.println(result1);
		
		Function<Double, Double> differntilate2 = differentilate(xsquare); //하나 이상의 함수를 인수로 받음, 함수를 결과로 반환
		double result2 = differntilate2.apply((double) 2);
		System.out.println(result2);
	}
	
	private static Function<Double, Double> differentilate(Function<Double, Double> func) {
		return func.andThen(x->2*x);
	}
	
	public static void main(String[] args) {
		FirstClassFunction();
		HigherOrderFunction();
	}
}
