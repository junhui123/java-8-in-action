package chapter14;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class LazyEvaluation {

	interface MyList<T> {
		T head();
		
		MyList<T> tail();
		
		default boolean isEmpty() {
			return true;
		}
		
		MyList<T> filter(Predicate<T> p);
	}
	
	static class MyLinkedList<T> implements MyList<T> {
		private final T head;
		private final MyList<T> tail;
		public MyLinkedList(T head, MyList<T> tail) {
			this.head = head;
			this.tail = tail;
		}

		@Override
		public T head() {
			return head;
		}

		@Override
		public MyList<T> tail() {
			return tail;
		}

		@Override
		public boolean isEmpty() {
			return false;
		}

		@Override
        public MyList<T> filter(Predicate<T> p) {
            return isEmpty() ? this : p.test(head()) ? new MyLinkedList<>(
                    head(), tail().filter(p)) : tail().filter(p);
        }
	}
	
	static class Empty<T> implements MyList<T> {
		@Override
		public T head() {
			throw new UnsupportedOperationException();
		}

		@Override
		public MyList<T> tail() {
			throw new UnsupportedOperationException();
		}
		
		@Override
        public MyList<T> filter(Predicate<T> p) {
            return this;
        }
	}
	
	static class LazyList<T> implements MyList<T> {
		final T head;
		final Supplier<MyList<T>> tail;
		public LazyList(T head, Supplier<MyList<T>> tail) {
			this.head = head;
			this.tail = tail;
		}
		
		@Override
		public T head() {
			return head;
		}
		
		@Override
		public MyList<T> tail() { 
			return tail.get(); //게으른 동작, get()를 호출해야 LazyList의 노드 생성
		}

		@Override
		public boolean isEmpty() {
			return false;
		}

		@Override
        public MyList<T> filter(Predicate<T> p) {
            return isEmpty() ? this : p.test(head()) ? new LazyList<>(head(),
                    () -> tail().filter(p)) : tail().filter(p);
        }
	}
	
	public static LazyList<Integer> from(int n) {
		return new LazyList<Integer>(n, ()->from(n+1));
	}
	
	public static MyList<Integer> primes(MyList<Integer> numbers) {
		Supplier<MyList<Integer>> s = ()->primes(numbers.tail().filter(n->n%numbers.head() != 0));
		return new LazyList<>(numbers.head(), s);
	}
	
	public static void pirnt(Object... objs) {
		Arrays.stream(objs).forEach(x -> System.out.print(x+" "));
		System.out.println();
	}
	
	public static <T> void printAll(MyList<T> list) {
		if(list.isEmpty()) 
			return;
		
		System.out.println(list.head());
		printAll(list.tail());
	}
	
	public static void Test_LazyList_NoFilter() {
		LazyList<Integer> numbers = from(2);
		int two = numbers.head();
		int three = numbers.tail().head();
		int four = numbers.tail().tail().head();
		pirnt(two, three, four);
	}
	
	public static void Test_LazyList_Filter() {
		LazyList<Integer> numbers = from(2);
		int two = primes(numbers).head();
		int three = primes(numbers).tail().head();
		int four = primes(numbers).tail().tail().head();
		pirnt(two, three, four);
	}
	
	public static void main(String[] args) {
		Test_LazyList_NoFilter();
		Test_LazyList_Filter();
		
		//스택 오버 플로우 발생 : JVM은 꼬리 재귀 최적화 지원하지 않음(보안상의 이유로..)
		//printAll(from(2)); //11423 까지 출력 Exception in thread "main" java.lang.StackOverflowError
		printAll(primes(from(2))); //74869 까지 출력 Exception in thread "main" java.lang.StackOverflowError
	}
}
