package chapter14;

import java.util.stream.IntStream;

//조급한 계산법(Eager evaluation) 혹은 엄격한 계산법(Strict evaluation)
public class EagerEvaluation {

	static IntStream numbers( ) {
		return IntStream.iterate(2,  n->n+1);
	}
	
	static int head(IntStream numbers) {
		return numbers.findFirst().getAsInt();
	}
	
	static IntStream tail(IntStream numbers) {
		return numbers.skip(1);
	}
	
	/**
	 * concat()에서 직접적으로 primes()를 재귀호출 진행 => 무한 재귀에 빠짐
	 * primes()를 lazy evaluation으로 호출하는 방식으로 해결 가능  (LazyEvaluation.java)
	 */
	static IntStream primes(IntStream numbers) {
		int head = head(numbers);
		return IntStream.concat(
								IntStream.of(head), 
								primes(tail(numbers).filter(n->n%head!=0))
							   );
	}
	
	/**
	 * IllegalStateException: stream has already been operated upon or closed
	 * => head(), tail() 에서 각각 findFirst(), skip(). 최종연산을 호출하여 스트림을 사용함
	 * 
	 * 스트림은 데이터를 저장하지 않고 1번만 사용함으로써 효율적으로 처리
	 */
	static void Test() {
		IntStream numbers = numbers();
		int head = head(numbers);
		IntStream filtred = tail(numbers).filter(n->n%head != 0);
	}
	
	public static void main(String[] args) {
		//Test();
		IntStream numbers = numbers();
		primes(numbers);
	}
}
