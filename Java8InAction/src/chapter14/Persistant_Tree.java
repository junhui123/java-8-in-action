package chapter14;

public class Persistant_Tree {

	//BST
	static class Tree {
		private String key;
		private int val;
		private Tree left, right;
		public Tree(String key, int val, Tree left, Tree right) {
			this.key = key;
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}
	
	static class TreeProcessor {
		public static int lookUp(String k, int defaultVal, Tree t) {
			if(t == null) return defaultVal;
			if(k.equals(t.key)) return t.val;
			return lookUp(k, defaultVal, k.compareTo(t.key)<0 ? t.left : t.right);
		}
		
		/**
		 * 기존 트리를 공유하며 누군가가 트리 갱신하는 경우 트리를 공유하는 사용자는 모두 영향 받음 
		 */
		public static Tree update(String k, int newval, Tree t) {
			if (t==null) {
				t = new Tree(k, newval, null, null);
			}
			else if(k.equals(t.key)) {
				t.val = newval;
			}
			else if(k.compareTo(t.key) < 0) {
				t.left = update(k, newval, t.left);
			} 
			else {
				t.right = update(k, newval, t.right);
			}
			return t;
		}
		
		/**
		 * 새로운 Tree를 반환. 모든 사용자가 같은 자료를 공유하며 갱신하는 Side-Effect 영향 받지 않음.
		 *  -> Persistant : 결과 자료 구조를 변경하지 말라. 저장된 값이 누군가에게 영향을 받지 않는 상태.
		 * 기존 구조를 변화시키지 않음. 
		 */
		public static Tree fupdate(String k, int newval, Tree t) {
			if(t==null) {
				return new Tree(k, newval, null, null);
			} 
			if(k.equals(t.key)) {
				return new Tree(k, newval, t.left, t.right);
			}
			if(k.compareTo(t.key) < 0) {
				return new Tree(t.key, t.val, fupdate(k, newval, t.left), t.right);
			} else {
				return new Tree(t.key, t.val, t.left, fupdate(k, newval, t.right));
			}
//			return (t==null) ?
//					new Tree(k, newval, null, null) :
//						k.equals(t.key) ?
//								new Tree(k, newval, t.left, t.right) :
//						k.compareTo(t.key) < 0 ?
//							new Tree(t.key, t.val, fupdate(k, newval, t.left), t.right) :
//							new Tree(t.key, t.val, t.left, fupdate(k, newval, t.right)) ;
		}
	}
	

	
	public static void main(String[] args) {
        Tree t = new Tree("Mary", 22,
                new Tree("Emily", 20,
                        new Tree("Alan", 50, null, null),
                        new Tree("Georgie", 23, null, null)
                ),
                new Tree("Tian", 29,
                        new Tree("Raoul", 23, null, null),
                        null
                )
        );

        // found = 23
        System.out.println(TreeProcessor.lookUp("Raoul", -1, t));
        // not found = -1
        System.out.println(TreeProcessor.lookUp("Jeff", -1, t));

        Tree f = TreeProcessor.fupdate("Jeff", 80, t);
        // found = 80
        System.out.println(TreeProcessor.lookUp("Jeff", -1, f));

        Tree u = TreeProcessor.update("Jim", 40, t);
        // t was not altered by fupdate, so Jeff is not found = -1
        System.out.println(TreeProcessor.lookUp("Jeff", -1, u));
        // found = 40
        System.out.println(TreeProcessor.lookUp("Jim", -1, u));

        Tree f2 = TreeProcessor.fupdate("Jeff", 80, t);
        // found = 80
        System.out.println(TreeProcessor.lookUp("Jeff", -1, f2));
        // f2 built from t altered by update() above, so Jim is still present = 40
        System.out.println(TreeProcessor.lookUp("Jim", -1, f2));
	}
}
