package chapter14;

import java.util.function.Consumer;


/**
 * 영속 자료구조 = 함수형 자료구조 = 불변 자료구조
 *  = 저장된 값이 다른 누군가에 의해 영향을 받지 않는 상태
 * (DB의 Persistant = DB는 프로그램 종료 후에도 남아 있음을 의미)
 * 
 * 전역 자로구조나 인수로 전달된 구조를 갱신  할 수 없음. 
 * 구조나 값을 변경한다면 같은 메소드를 두 번 호출시 결과가 달라짐 -> 참초 투명성 위배
 * 인수를 결과로 단순하게 매핑할 수 없음
 * 
 */
public class Persistant_TrainJourney {

	/**
	 * A역에서 B역까지의 기차 여행을 의미하는 단방향 연결 리스트
	 */
	static class TrainJourney {
		public int price;
		public TrainJourney onward;
		public TrainJourney(int price, TrainJourney ownard) {
			this.price = price;
			this.onward = ownard;
		}
		
		/**
		 * a = x->y로 가는 여행
		 * b = y->z로 가는 여행
		 * link 호출 시 a가 갱신되어 x->y->z로 가는 Side-Effect 발생
		 *  =>파괴적 갱신
		 */ 
		static TrainJourney link(TrainJourney a, TrainJourney b) {
			if(a==null) return b;
			TrainJourney t = a;
			while(t.onward != null) {
				t = t.onward;
			}
			t.onward=b;
			return a;
		}
		
		/**
		 * 자료구조를 갱신하지 않도록 새로운 자료구조를 만들어 반환
		 * 함수형 및 객체지향 관점에서도 좋은 방법
		 */
		static TrainJourney append(TrainJourney a, TrainJourney b) {
			return a==null ? b : new TrainJourney(a.price, append(a.onward, b));
		}
		
		static void visit(TrainJourney journey, Consumer<TrainJourney> c) {
	        if (journey != null) {
	            c.accept(journey);
	            visit(journey.onward, c);
	        }			
		}
	}
	
	public static void main(String[] args) {
        TrainJourney tj1 = new TrainJourney(40, new TrainJourney(30, null));
        TrainJourney tj2 = new TrainJourney(20, new TrainJourney(50, null));

        TrainJourney appended = TrainJourney.append(tj1, tj2);
        TrainJourney.visit(appended, tj -> { System.out.print(tj.price + " - "); });
        System.out.println();

        // A new TrainJourney is created without altering tj1 and tj2.
        TrainJourney appended2 = TrainJourney.append(tj1, tj2);
        TrainJourney.visit(appended2, tj -> { System.out.print(tj.price + " - "); });
        System.out.println();

        // tj1 is altered but it's still not visible in the results.
        TrainJourney linked = TrainJourney.link(tj1, tj2);
        TrainJourney.visit(linked, tj -> { System.out.print(tj.price + " - "); });
        System.out.println();		
	}
	
}
